<?php

include("include/header.php");

?>

<style>

   @import url('https://fonts.googleapis.com/css?family=Playfair+Display&display=swap');

</style>



<!-- HOME SLIDER -->

<!-- Go to js/init.js line 17 to add slider images -->
<div class="home-slider">

   <div class="intro-text container">

      <h1 class="slider-slogan">Experience the real</h1>

      <span class="luxury">LUXURY</span>

      <span class="stars"><img class="singleStar firstStar" src="images/home/Star.svg"><img class="singleStar" src="images/home/Star.svg"><img class="singleStar" src="images/home/Star.svg"><img class="singleStar" src="images/home/Star.svg"><img class="singleStar lastStar" src="images/home/Star.svg"></span>



      <!-- <a href="reservation.php" class="view-more more-white">Book a Table</a> -->

   </div>

   <div class="scrollDown">

      <a href="#home-content-1" class="scrollDownLink"><span class="arrowDown"></span></a>

   </div>

</div>

<!-- /HOME SLIDER -->

<section id="home-content-1" class="home-widget home-features">

   <div class="container welcomeContainer">

      <h3 class="welcomeTitle">Welcome to<br class="breakResponsive"> Maélys Restaurant</h3>

      <p class="welcomeContent">Our restaurant offers a wide range of delicious foods prepared with great love and passion by the best master chefs in town. <br>All the ingredients are carefully selected with the highest quality and freshness.</p>

      <span class="stars"><img class="singleStar firstStar" src="images/home/Star.svg"><img class="singleStar" src="images/home/Star.svg"><img class="singleStar" src="images/home/Star.svg"><img class="singleStar" src="images/home/Star.svg"><img class="singleStar lastStar" src="images/home/Star.svg"></span>

   </div>

   <!--.container-->

</section>

<!-- SECTION 2 -->

<section id="home-content-2" class="home-widget">

   <div class="container aboutUsContainer">

      <div class="row">



         <div class="col-md-6">

            <!-- <h2 class="home-subtitle">History</h2> -->

            <div class="centerMobile">

               <h1 class="home-title margin-b24 title-headline">About us</h1>

            </div>

            <!-- <div class="title-style-1">Welcome to Maelys, a modern restaurant with a focus on premium food tastes</div> -->

            <div class="aboutUsSpan">

               <p class="aboutUsParagraph">Located in the heart of the city of Deçan, Maélys Restaurant & Irish Bar offers a breathtaking view of the famous Albanian Alps. From the seductively royal interior to the marvelous food & drinks, our restaurants vibrant ambiance is as alluring as the cuisine itself.

                  <br><br>



                  We offer a unique food concept, featuring the best appetizers, fresh market fish, exceptional seafood and mouth-watering steak. Another unique detail of our restaurant is the best imported liquours & cocktails from Germany, a characteristic that seperates our restaurant from the others.

               </p>

               <!-- <img class="ceoSignature" src="images/home/ceoSignature.svg"> -->

               <!-- <a href="about-us.php" id="aboutUsLink" class="view-more margin-t36">Read More</a> -->

            </div>

         </div>

         <!-- /col-md-6 -->

         <!-- <div class="col-md-7">

            <img class="img-responsive img-feature" src="images/home/about-7.jpg" alt="about-7" />

         </div> -->

         <!-- /col-md-6 -->



      </div>

      <!-- /row-->

   </div>

   <!-- /container-->

</section>

<!-- /SECTION 2 -->



<!-- SECTION 3 -->

<!-- Add parallax class to a section and a background image to create the parallax effect -->

<section class="home-widget parallax" style="background-image:url('images/home/about-4.jpg');">

   <div class="parallax-content">

      <div class="container">

         <div class="row">



            <div class="col-md-12">

               <div class="img-center alignc">

                  <img class="img-responsive img-feature foodSlogan" src="images/home/FoodSlogan.svg" alt="taste-food" />

                  <!-- <h3 class="tasteFood">Enjoy the five star food</h3> -->

               </div>

            </div>

            <!--.col-md-12-->



         </div>

         <!--.row-->

      </div>

      <!--.container-->

   </div>

   <!-- /parallax-content -->

</section>

<!-- /SECTION 3 -->



<!-- SECTION 4 -->

<section id="home-content-4" class="home-widget">

   <div class="container">

      <div class="row">



         <div class="col-md-12 alignc">

            <!-- <h2 class="home-subtitle">Discover</h2> -->

            <h1 class="home-title margin-b24 title-headline menuH1">Our Menu</h1>

            <p class="menuParagraph">Our menus highlight our unique focus on where our food comes from. Whether it's mouth-watering steak from our butcher, sustainably sourced seafood, each ingredient is transformed into something delightfully delicious. Enjoy!</p>

            <div class="MenuCategory">

               <div class="row menuRow">

                  <div class="col-md-4 menuCol firstMenuCol">

                     <div class="hoverImage">

                        <a href="menu.php#coursesImage" class="toMenu"><img src="images/home/FoodImage.jpg"></a>

                     </div>

                     <h3 class="menuTitle">Main Courses</h3>



                  </div>

                  <div class="col-md-4 menuCol secondMenuCol">

                     <div class="hoverImage">

                        <a href="menu.php#drinksImage" class="toMenu" id="indexDrinks"><img src="images/home/DrinksImage.jpg"></a>

                     </div>

                     <h3 class="menuTitle">Drinks</h3>



                  </div>

                  <div class="col-md-4 menuCol thirdMenuCol">

                     <div class="hoverImage">

                        <a href="menu.php#dessertImage" class="toMenu"><img src="images/home/DessertsImage.jpg"></a>

                     </div>

                     <h3 class="menuTitle">Desserts</h3>

                  </div>

               </div>

               <!-- FOOD MENU -->

               <ul class="our-menu">

               </ul>

               <!-- /FOOD MENU-->



               <a href="menu.php" class="view-more margin-t36 goToMenu" id="goToMenu">MENU</a>



            </div>

            <!--.col-md-12-->

         </div>

         <!--.row-->

      </div>

      <!--.container-->

</section>

<!-- /SECTION 4 -->



<!-- SECTION 5 -->

<!-- Add parallax class to a section and a background image to create the parallax effect -->

<section class="home-widget parallax" style="background-image:url('images/home/parallax-2.jpg');">

   <div class=backgroundOverlay></div>

   <div class="parallax-content">

      <div class="container">

         <div class="row">

            <img class="luxDesignImg" src="images/home/LuxuriousInteriorDesign.svg" />

         </div>

         <!--.row-->

      </div>

      <!--.container-->

   </div>

   <!-- parallax-content -->

</section>

<!-- /SECTION 5 -->

<!-- SECTION 2 -->

<section id="home-content-6" class="home-widget">

   <div class="container aboutUsContainer">

      <div class="row WinesRow">



         <div class="col-md-6 winesDiv">

            <!-- <h2 class="home-subtitle">History</h2> -->

            <div class="centerMobile">

               <h1 class="home-title margin-b24 title-headline winesH1">Unique Drinks</h1>

            </div>

            <div class="WinesSpan">

               <p class="WinesParagraph">We offer you the most unique drinks, especially a numerous list of alcoholic drinks with original taste, exclusively imported from Germany and served in a luxury way on our Restaurant and Irish Bar. We invite you to come and enjoy our drinks that you cannot enjoy anywhere else.

               </p>

            </div>

         </div>

      </div>

      <!-- /row-->

   </div>

   <!-- /container-->

</section>

<!-- /SECTION 2 -->



<section id="home-content-end" class="home-widget home-features">

   <div class="container welcomeContainer endContainer">

      <h3 class="welcomeTitle quotes">“</h3>

      <!-- <h3 class="welcomeTitle">Welcome to Maélys Restaurant</h3> -->

      <p class="welcomeContent quoteContent">Life is all about having a good time with your loved ones.</p>

      <img class="ceoSignature endPage" src="images/home/ceoSignature.svg">



   </div>

   <!--.container-->

</section>



<?php

include("include/footer.php");

?>