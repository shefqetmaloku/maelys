<div id="myCookieConsent">
	
	<div>This&nbsp;website uses cookies to ensure you get the best experience on our website. <a href="impressum.php">More&nbsp;details</a><a id="cookieButton">I Agree</a></div>
</div>
<footer class="pageFooter">
    <div class="container">
        <!-- FOOTER LOGO -->
        <!-- <div class="footer-logo">
            <img class="img-responsive MaelysSymbol" src="images/home/MaelysSymbol.svg" alt="" />
        </div> -->

        <!-- ROW -->
        <div class="row alignc row-infos">

            <!-- FOOTER COLUMN 1 -->
            <div class="col-md-4">
                <div class="footer-content">
                    <img class="footerIcons" src="images/logo/MaelysFooter.svg">
                    <h5 class="infoTitle">Find us on social Media:</h5>
                    <ul class="footer-social">
                        <li><a id="fb" class="social-facebook" href="https://www.facebook.com/maelysrestaurant/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <!-- <li><a class="social-twitter" href="https://twitter.com/matchthemes" target="_blank"><i class="fa fa-twitter"></i></a></li> -->
                        <!-- <li><a class="social-gplus" href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
            <li><a class="social-pinterest" href="#" target="_blank"><i class="fa fa-pinterest"></i></a></li> -->
                        <li><a id="insta" class="social-instagram" href="https://www.instagram.com/maelys.restaurant/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                    <!-- <p class="infoParagraph">Str. "Dëshmorët e Kombit" <br />
                        51000, Deçan, Kosovo
                    </p> -->
                </div>
                <!-- <div class="footer-content">
                <img class="footerIcons" src="images/home/footerIcons/placeholder.svg">
                    <h5 class="infoTitle">ADDRESS:</h5>
                    <p class="infoParagraph">Str. "Dëshmorët e Kombit" <br />
                        51000, Deçan, Kosovo
                    </p>
                </div> -->
            </div>
            <div class="col-md-4">
                <div class="footer-content">
                    <!-- <img class="footerIcons" src="images/home/footerIcons/wall-clock.svg"> -->
                    <h5 class="infoTitle FooterMenuTitle">QUICK LINKS:</h5>
                    <ul class="FooterMenu">
                        <li class="FooterMenuItem"><a href="">HOME</a></li>
                        <li class="FooterMenuItem"><a href="">MENU</a></li>
                        <li class="FooterMenuItem"><a href="">RESERVATION</a></li>
                        <li class="FooterMenuItem"><a href="">GALLERY</a></li>
                        <li class="FooterMenuItem"><a href="">CONTACT</a></li>
                        <li class="FooterMenuItem"><a href="impressum.php">IMPRESSUM</a></li>
                    </ul>
                    <!-- <p class="infoParagraph">Monday - Saturday:<br /> 9:00 - 23:59
                    </p> -->
                </div>
            </div>

            <!-- FOOTER COLUMN 2 -->
            <div class="col-md-4">
                <div class="footer-content">
                    <!-- <img class="footerIcons" src="images/home/footerIcons/telephone.svg"> -->
                    <h5 class="infoTitle contactFooter">CONTACT US:</h5>
                    <ul class="FooterMenu addressUl">
                        <li class="FooterMenuItem"><p class="infoParagraph address">Str. "Dëshmorët e Kombit"</p></li>
                        <li class="FooterMenuItem"><p class="infoParagraph address">51000, Deçan, Kosovo</p></li>
                        <li class="FooterMenuItem"><a href="tel:+38349111369">+383 (0) 49 111 369</a></li>
                        <li class="FooterMenuItem"><a href="mailto:info@maelys-ks.com">info@maelys-ks.com</a></li>
                    </ul>
                    <!-- <p class="infoParagraph address">Str. "Dëshmorët e Kombit" <br />
                        51000, Deçan, Kosovo<br/><br class="footerBr"/>+383 (0) 49 111 369<br /><br/>
                        
                    </p> -->
                </div>
            </div>

            <!-- FOOTER COLUMN 3 -->

        </div>
        <!-- /ROW -->

        <!-- FOOTER SCROLL UP -->
        <div class="scrollup">
            <a class="scrolltop" href="#">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
        <!-- /FOOTER SCROLL UP -->

    </div>
    <!--container-->
    <!-- FOOTER COPYRIGHT -->
    <div class="copyright">
        <span class="copyrightText">
            &copy; 2019, Maélys Restaurant & Irish Bar</span>
    </div>
    <!-- /FOOTER COPYRIGHT -->
</footer>
<!-- /FOOTER -->

<!-- JS -->
<script type='text/javascript' src='js/jquery.js'></script>
<script type='text/javascript' src='js/jquery-migrate.min.js'></script>
<script type='text/javascript' src='css/bootstrap/js/bootstrap.min.js'></script>
<script type='text/javascript' src='js/jquery.easing.min.js'></script>
<script type='text/javascript' src='js/jquery.fitvids.js'></script>
<script type='text/javascript' src='js/prettyphoto/js/jquery.prettyPhoto.js'></script>
<script type='text/javascript' src='js/vegas.min.js'></script>
<!-- MAIN JS -->
<script type='text/javascript' src='js/init.js'></script>
</body>

</html>