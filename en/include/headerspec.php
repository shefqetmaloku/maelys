<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Maelys Restaurant & Irish Bar</title>
    <meta name="robots" content="noodp" />
    <!-- Google Fonts -->
    <link rel='stylesheet' id='dina-body-font-css' href='http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' type='text/css' media='all' />
    <link rel='stylesheet' id='dina-post-titles-font-css' href='http://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700' type='text/css' media='all' />
    <!-- Bootstrap CSS -->
    <link rel='stylesheet' id='dina-bootstrap-css-css' href='css/bootstrap/css/bootstrap.min.css' type='text/css' media='all' />
    <!-- Font Awesome Icons CSS -->
    <link rel='stylesheet' id='dina-font-awesome-css' href='css/fontawesome/css/font-awesome.min.css' type='text/css' media='all' />
    <!-- Lightbox PrettyPhoto CSS -->
    <link rel='stylesheet' id='dina-pretty-css-css' href='js/prettyphoto/css/prettyPhoto.css' type='text/css' media='all' />
    <!-- Main CSS File -->
    <link rel='stylesheet' id='dina-style-css-css' href='css/style.css' type='text/css' media='all' />
    <!-- Slider CSS -->
    <link rel='stylesheet' id='dina-vegas-css-css' href='css/vegas.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/menu.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/custom.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/gallery.css' type='text/css' media='all' />

    <!-- favicons -->
    <link rel="icon" href="images/icons/maelysfavicon.png" sizes="32x32" />
    <link rel="icon" href="images/icons/maelysfavicon.png" sizes="192x192" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <link rel="apple-touch-icon-precomposed" href="images/icons/favicon-180x180.png" />
    <!-- <script src="js/script.js"> -->
    <script src="js/script.js"></script>
</head>

<body class="body-header1">
    <div class="slider-overlay"></div>
    <div id="menu-links-header">
        <div class="menu-background-color"></div>
        <div id="menu-container" class="container">
            <div class="row">
                <div class="col-sm-6 menuCol">
                    <ul id="menu-links">
                        <li class="links"><a class="menu-item" href="index.php">Home</a></li>
                        <li class="links"><a class="menu-item" href="menu.php">Menu</a></li>
                        <li class="links"><a class="menu-item" href="reservation.php">Reservation</a></li>
                        <li class="links"><a class="menu-item" href="menu-gallery.php">Gallery</a></li>
                        <li class="links"><a class="menu-item" href="contact.php">Contact</a></li>
                    </ul>
                    <div class="book-now-header mobileDisplay">
                        <a href="reservation.php" class="book-now-link">Book a table</a>
                    </div>
                    <div class="languages">
                        <div class="row langRow">
                            <div class="col-md-1 alb">
                                <a class="albLang" href="index.php">ALB</a>
                                <!-- <p class="albLang">ALB</p> -->
                            </div>  
                            <div class="col-md-1 lineSeperate">
                                <p class="lineBetween">|</p>
                            </div>



                            <div class="col-md-1 eng">
                                <a class="engLang active" href="index_EN.php">ENG</a>
                                <!-- <p class="engLang">ENG</p> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6"></div>
            </div>
        </div>
    </div>
    <header id="section-header" class="desktopMenu">
        <div class="row header-row specialCase" id="menuBottom">
            <div class="col-sm-4 left-col">
                <div class="hamburger-menu">
                    <div class="icon">
                        <div style="flex-direction: column;height: 20px;justify-content: space-around;display: flex;">
                            <div class="hamb top"></div>
                            <div class="hamb middle"></div>
                        </div>

                    </div>
                </div>
                
            </div>
            <div class="col-sm-4 mid-col">
                <div class="logo-image">
                    <div class="logo-holder">
                        <a href="http://localhost/maelys/">
                            <img class="d1se_header" src="images/logo/Maelys_Logo.svg" alt="" height="38">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 right-col">
                <div class="book-now-header">
                    <a href="reservation.php" class="book-now-link">Book a table</a>
                </div>
            </div>
        </div>

    </header>
    <header id="section-header" class="mobileMenu">
        <div class="row header-row specialCase" id="menuBottomMobile">
            <div class="col-sm-4 left-col mobileLogo">
                <div class="logo-image">
                    <div class="logo-holder">
                        <a href="http://localhost/maelys/">
                            <img class="d1se_header" src="images/logo/Maelys_Logo.svg" alt="" height="38">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 mid-col mobileHam">
                <div class="hamburger-menu">
                    <div class="icon">
                        <div style="flex-direction: column;height: 20px;justify-content: space-around;display: flex;">
                            <div class="hamb top"></div>
                            <div class="hamb middle"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </header>