<?php
include("include/header.php"); ?>

<style>
    <?php include 'css/custom.css'; ?>
</style>

<!-- TOP IMAGE HEADER -->
<section class="topSingleBkg topPageBkg">
    <div class="item-content-bkg">
        <div class="item-img" style="background-image:url('images/top-headers/Contactusimage.jpg');"></div>
        <div class="inner-desc">
        <img class="bookNowImg contactUsSvg" src="images/top-headers/Kontakti-02.svg">
        </div>
    </div>
</section>

<!-- MAIN WRAP CONTENT -->
<section id="wrap-content" class="page-content contactContent">
    <div class="container">
        <div class="row contactRow">
            <div class="col-md-12">

                <div class="page-holder custom-page-template">
                    <div class="row">
                        <div class="headline ContactUsTitle">
                            <h2>Na kontaktoni</h2>
                        </div>
                        <p class="getInTouchP">Nëse keni ndonjë këshillë apo sygjerim, ju lutemi ta ndani me ne. Ndjehuni të lirë të na kontaktoni.</p>

                        <!-- LEFT CONTENT -->
                        <div class="col-md-6">

                            <div id="contact-form-holder">
                                <form method="post" id="contact-form" action='include/contact-process.php'>
                                    <p><input type="text" name="name" class="reservation-fields contactFields" placeholder="Emri*" required oninvalid="this.setCustomValidity('Ju lutem shënoni emrin!')" oninput="this.setCustomValidity('')" /></p>
                                    <p><input type="email" name="email" class="reservation-fields contactFields" placeholder="Email*" required oninvalid="this.setCustomValidity('Ju lutem shënoni emailin në formën email@domain!')" oninput="this.setCustomValidity('')"/></p>
                                    <p><input type="text" name="subject" class="reservation-fields contactFields" placeholder="Tema*" required oninvalid="this.setCustomValidity('Ju lutem shënoni temën!')" oninput="this.setCustomValidity('')" /></p>  
                                    <p> <textarea name="message" id="msg-contact" class="reservation-fields" rows="7" placeholder="Mesazhi*" required oninvalid="this.setCustomValidity('Ju lutem shënoni mesazhin!')" oninput="this.setCustomValidity('')"></textarea></p>
                                    <p class="antispam">Leave this empty: <input type="text" name="url" /></p>
                                    <p class="contact-btn"><input type="submit" value="Dërgo" id="submit" class="submitContact" /></p>
                                </form>
                            </div>
                            <!-- contact-form-holder-->
                            <div id="output-contact"></div>

                        </div>
                        <!-- /LEFT CONTENT -->

                        <!-- RIGHT CONTENT -->
                        <div class="col-md-6" id="contactPageInfos">
                            <ul>
                                <li><p class="contactAddressItem">Adresa: Rr. "Dëshmorët e Kombit"</p></li>
                                <li><p class="contactAddressItem">51000 Deçan, Kosovë</p></li>
                                <li><p class="contactAddressItem">Phone: +383 (0) 49 111 369</p></li>
                                <li><p class="contactAddressItem">Email: <a class="contactEmail" href="mailto:info@maelys-ks.com">info@maelys-ks.com</a></p> </li>
                            </ul>
                            <div class="mapouter">
                                <div class="gmap_canvas"><iframe width="700" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q=Ma%C3%A9lys%20Restaurant%20%26%20Irish%20Bar%2C%2051000%20Sheshi%20Mentor%20Tolaj%2C%20De%C3%A7an&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.embedgooglemap.org"></a></div>
                                <style>
                                    .mapouter {
                                        position: relative;
                                        text-align: right;
                                        max-height: 320px;
                                        max-width: 560px;
                                        margin-top: 23px;
                                    }

                                    .gmap_canvas {
                                        overflow: hidden;
                                        background: none !important;
                                        max-height: 320px;
                                        max-width: 560px;
                                    }
                                </style>
                            </div>

                            <!--/gmaps-->

                        </div>
                        <!-- /RIGHT CONTENT -->

                    </div>
                    <!--/row-->
                </div>
                <!-- /page-holder -->
            </div>
            <!--col-md-12-->
        </div>
        <!--row-->
    </div>
    <!--container-->
</section>
<!-- /MAIN WRAP CONTENT -->

<?php
include "include/footer.php";
?>