<div id="myCookieConsent">
	
	<div>Kjo&nbsp;faqe përdor biskota të cilat ju mundësojnë ekpseriencën më të mirë në faqen tonë<a href="impressum.php">Më&nbsp;shumë</a><a id="cookieButton">Pajtohem</a></div>
</div>
<footer class="pageFooter">
    <div class="container">

        <!-- ROW -->
        <div class="row alignc row-infos">

            <!-- FOOTER COLUMN 1 -->
            <div class="col-md-4">
                <div class="footer-content">
                    <img class="footerIcons" src="images/logo/MaelysFooter.svg">
                    <h5 class="infoTitle">Na ndiqni në rrjetet sociale:</h5>
                    <ul class="footer-social">
                        <li><a id="fb" class="social-facebook" href="https://www.facebook.com/maelysrestaurant/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a id="insta" class="social-instagram" href="https://www.instagram.com/maelys.restaurant/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    </ul>

                </div>

            </div>
            <div class="col-md-4">
                <div class="footer-content">
                    <h5 class="infoTitle FooterMenuTitle" id="sqNavigimi">NAVIGIMI:</h5>
                    <ul class="FooterMenu">
                        <li class="FooterMenuItem"><a href="index.php">BALLINA</a></li>
                        <li class="FooterMenuItem"><a href="menu.php">MENU</a></li>
                        <li class="FooterMenuItem"><a href="reservation.php">REZERVO</a></li>
                        <li class="FooterMenuItem"><a href="menu-gallery.php">GALERIA</a></li>
                        <li class="FooterMenuItem"><a href="contact.php">KONTAKTI</a></li>
                        <li class="FooterMenuItem"><a href="impressum.php">IMPRESUMI</a></li>
                    </ul>
                </div>
            </div>

            <!-- FOOTER COLUMN 2 -->
            <div class="col-md-4">
                <div class="footer-content">
                    <h5 class="infoTitle contactFooter" id="sqKontakti">KONTAKTI</h5>
                    <ul class="FooterMenu addressUl">
                        <li class="FooterMenuItem"><p class="infoParagraph address">Rr. "Dëshmorët e Kombit"</p></li>
                        <li class="FooterMenuItem"><p class="infoParagraph address">51000, Deçan, Kosovo</p></li>
                        <li class="FooterMenuItem"><a href="tel:+38349111369">+383 (0) 49 111 369</a></li>
                        <li class="FooterMenuItem"><a href="mailto:info@maelys-ks.com">info@maelys-ks.com</a></li>
                    </ul>
                </div>
            </div>

            <!-- FOOTER COLUMN 3 -->

        </div>
        <!-- /ROW -->

        <!-- FOOTER SCROLL UP -->
        <div class="scrollup">
            <a class="scrolltop" href="#">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
        <!-- /FOOTER SCROLL UP -->

    </div>
    <!--container-->
    <!-- FOOTER COPYRIGHT -->
    <div class="copyright">
        <span class="copyrightText">
            &copy; 2019, Maélys Restaurant & Irish Bar</span>
    </div>
    <!-- /FOOTER COPYRIGHT -->
</footer>
<!-- /FOOTER -->

<!-- JS -->
<script type='text/javascript' src='js/jquery.js'></script>
<script type='text/javascript' src='js/jquery-migrate.min.js'></script>
<script type='text/javascript' src='css/bootstrap/js/bootstrap.min.js'></script>
<script type='text/javascript' src='js/jquery.easing.min.js'></script>
<script type='text/javascript' src='js/jquery.fitvids.js'></script>
<script type='text/javascript' src='js/prettyphoto/js/jquery.prettyPhoto.js'></script>
<script type='text/javascript' src='js/vegas.min.js'></script>
<!-- MAIN JS -->
<script type='text/javascript' src='js/init.js'></script>
</body>

</html>