<!DOCTYPE html>

<html lang="en-US">



<head>

    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>Maelys Restaurant & Irish Bar</title>
    <!--Meta-->
    <meta name="description" content="Maélys Restaurant & Irish bar ✚ Restaurant luksoz me ushqim të shijshëm dhe pije unike. ✚ info@maelys-ks.com ✚ +383 (0) 49 111 369 " />
    <meta name="robots" content="index, follow" />
    <meta name="keywords" content="maélys restaurant & irish bar, maélys, restaurant, bar, irish bar, restaurant near me, delicious food, pizza, beef, pasta, deqan, best restaurant" />
    <meta itemprop="copyrightHolder" content="Maélys">
    <meta itemprop="copyrightYear" content="2019">
    <meta itemprop="image" content="https://maelys-ks.com/images/logo/Maelys_Logo.svg">


    <!--OG Tags-->

    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Maélys Restaurant">
    <meta property="og:title" content="Maélys Restaurant">
    <meta property="og:description" content="Maélys Restaurant & Irish bar ✚ Restaurant luksoz me ushqim të shijshëm dhe pije unike. ✚ info@maelys-ks.com ✚ +383 (0) 49 111 369  ">
    <meta property="og:image" content="https://maelys-ks.com/images/logo/Maelys_Logo.svg">

    <!--Twitter Tags-->
    <meta name="twitter:card" content="website">
    <meta name="twitter:site" content="Maélys Restaurant">
    <meta name="twitter:title" content="Maélys Restaurant">
    <meta name="twitter:description" content="Maélys Restaurant & Irish bar ✚ Restaurant luksoz me ushqim të shijshëm dhe pije unike. ✚ info@maelys-ks.com ✚ +383 (0) 49 111 369  ">
    <meta name="twitter:image" content="https://maelys-ks.com/images/logo/Maelys_Logo.svg">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="apple-touch-icon-precomposed" href="https://maelys-ks.com/images/logo/Maelys_Logo.svg">

    <!--Apple-->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="Maélys Restaurant">

    <!-- Google Fonts -->

    <link rel='stylesheet' id='dina-body-font-css' href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' type='text/css' media='all' />

    <link rel='stylesheet' id='dina-post-titles-font-css' href='https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700' type='text/css' media='all' />

    <!-- Bootstrap CSS -->

    <link rel='stylesheet' id='dina-bootstrap-css-css' href='../css/bootstrap/css/bootstrap.min.css' type='text/css' media='all' />

    <!-- Font Awesome Icons CSS -->

    <link rel='stylesheet' id='dina-font-awesome-css' href='../css/fontawesome/css/font-awesome.min.css' type='text/css' media='all' />

    <!-- Lightbox PrettyPhoto CSS -->

    <link rel='stylesheet' id='dina-pretty-css-css' href='../js/prettyphoto/css/prettyPhoto.css' type='text/css' media='all' />

    <!-- Main CSS File -->

    <link rel='stylesheet' id='dina-style-css-css' href='../css/style.css' type='text/css' media='all' />

    <!-- Slider CSS -->

    <link rel='stylesheet' id='dina-vegas-css-css' href='../css/vegas.css' type='text/css' media='all' />

    <link rel='stylesheet' href='../css/menu.css' type='text/css' media='all' />

    <link rel='stylesheet' href='../css/custom.css' type='text/css' media='all' />

    <link rel='stylesheet' href='../css/gallery.css' type='text/css' media='all' />
    <link rel='stylesheet' href='../css/datepicker.css' type='text/css' media='all' />


    <!--Animation AOS CSS-->

    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <!--Animation AOS JS-->

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>



    <!-- favicons -->

    <link rel="icon" href="../images/icons/maelysfavicon.png" sizes="32x32" />

    <link rel="icon" href="../images/icons/maelysfavicon.png" sizes="192x192" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <link rel="apple-touch-icon-precomposed" href="../images/icons/favicon-180x180.png" />

    <script src="../js/script.js"></script>
    <script type='text/javascript' src='js/datepicker.js'></script>


</head>



<body class="body-header1">

    <div class="slider-overlay"></div>

    <div id="menu-links-header">

        <div class="menu-background-color"></div>

        <div id="menu-container" class="container">

            <div class="row">

                <div class="col-sm-6 menuCol">

                    <ul id="menu-links">

                        <li class="links"><a class="menu-item" href="index.php">Ballina</a></li>

                        <li class="links"><a class="menu-item" href="menu.php">Menu</a></li>

                        <li class="links"><a class="menu-item" href="reservation.php">Rezervo</a></li>

                        <li class="links"><a class="menu-item" href="menu-gallery.php">Galeria</a></li>

                        <li class="links"><a class="menu-item" href="contact.php">Kontakti</a></li>

                    </ul>

                    <div class="book-now-header mobileDisplay">

                        <a href="reservation.php" class="book-now-link">Rezervo</a>

                    </div>

                </div>

                <div class="col-sm-6"></div>

            </div>

        </div>

    </div>

    <header id="section-header" class="desktopMenu">
        <div class="topInfosHeader" id="topBarDesktop">
            <div class="container topInfosHeader-container">
                <div class="row topInfosHeader-row">
                    <div class="col-sm-4 headerInfos-col" id="infos-1">
                        <div class="topInfosHolder">
                            <a href="tel:+38349111369">+383 (0) 49 111 369 </a>
                        </div>
                    </div>
                    <div class="col-sm-4 headerInfos-col" id="infos-2">
                        <div class="topInfosHolder">
                            <a href="mailto:info@maelys-ks.com">info@maelys-ks.com</a>
                        </div>
                    </div>
                    <div class="col-sm-4 headerInfos-col" id="infos-3">
                        <div class="topInfosHolder" id="infos-3-lang">
                            <div class="row languagesNewRow">
                                <div class="col-sm-1 col-eng"><a class="engLang" href="../index.php">ENG</a></div>
                                <div class="col-sm-1 col-line">
                                    <p id="langLine">|</p>
                                </div>
                                <div class="col-sm-1 col-alb"><a class="albLang active" href="index.php">ALB</a></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row header-row specialCase" id="menuBottom">

            <div class="col-sm-4 left-col">

                <div class="hamburger-menu">

                    <div class="icon">

                        <div style="flex-direction: column;height: 20px;justify-content: space-around;display: flex;">

                            <div class="hamb top"></div>

                            <div class="hamb middle"></div>

                        </div>



                    </div>

                </div>



            </div>

            <div class="col-sm-4 mid-col">

                <div class="logo-image">

                    <div class="logo-holder">

                        <a href="index.php">

                            <img class="d1se_header" src="../images/logo/Maelys_Logo.svg" alt="" height="38">

                        </a>

                    </div>

                </div>

            </div>

            <div class="col-sm-4 right-col">

                <div class="book-now-header">

                    <a href="reservation.php" class="book-now-link">Rezervo</a>

                </div>

            </div>

        </div>



    </header>

    <header id="section-header" class="mobileMenu">
        <div class="topInfosHeader" id="topBarMobile">
            <div class="container topInfosHeader-container">
                <div class="row topInfosHeader-row" id="mobileLangRow">
                    <div class="col-sm-4 headerInfos-col" id="infos-1">
                        <div class="topInfosHolder">
                            <a href="tel:+38349111369">+383 (0) 49 111 369 </a>
                        </div>
                    </div>
                    <div class="col-sm-4 headerInfos-col" id="infos-2">
                        <div class="topInfosHolder">
                            <a href="mailto:info@maelys-ks.com">info@maelys-ks.com</a>
                        </div>
                    </div>
                    <div class="col-sm-4 headerInfos-col" id="infos-3">
                        <div class="topInfosHolder" id="infos-3-lang">
                            <div class="row languagesNewRow" id="mobRow">
                                <div class="col-sm-1 col-eng"><a class="engLang" href="../index.php">ENG</a></div>
                                <div class="col-sm-1 col-line">
                                    <p id="langLine">|</p>
                                </div>
                                <div class="col-sm-1 col-alb"><a class="albLang active" href="index.php">ALB</a></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row header-row specialCase" id="menuBottomMobile">

            <div class="col-sm-4 left-col mobileLogo">

                <div class="logo-image">

                    <div class="logo-holder">

                        <a href="index.php">

                            <img class="d1se_header" src="../images/logo/Maelys_Logo.svg" alt="" height="38">

                        </a>

                    </div>

                </div>

            </div>

            <div class="col-sm-4 mid-col mobileHam">

                <div class="hamburger-menu">

                    <div class="icon">

                        <div style="flex-direction: column;height: 20px;justify-content: space-around;display: flex;">

                            <div class="hamb top"></div>

                            <div class="hamb middle"></div>

                        </div>

                    </div>

                </div>

            </div>

        </div>



    </header>