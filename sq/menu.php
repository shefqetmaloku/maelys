<?php
include("include/headerspec.php"); ?>

<section class="ourMenuSection">
    <div class="container ourMenuDiv">
        <h1 class="menuLeftTitle">Menu</h3>
            <p class="ourMenuParagraph">Menuja jonë vë në pah fokusin tonë unik në përbërësit e ushqimit, qoftë ai bifteku i jashtëzakonshëm i zgjedhur nga kasapi ynë apo nga ushqimi i freskët i detit, çdo përbërës transformohet në diçka tepër të shijshme. Shijojeni!</p>
    </div>
</section>


<!-- MAIN WRAP CONTENT -->
<section class="menu-1col-content" id="menuSection">

    <div class="col-md-12 leftDiv starters">
        <div class="menuBackgroundDiv leftDiv" id="appetizersDiv"><img class="menuTopImage" src="images/menu/Appetizers.jpg"></div>
    </div>
    <div class="col-md-12 leftDiv salads">
        <div class="menuBackgroundDiv leftDiv" id="saladsDiv"><img class="menuTopImage" src="images/menu/Salads.jpg"></div>
    </div>
    <div class="col-md-12 leftDiv mainCourses">
        <div class="menuBackgroundDiv leftDiv" id="coursesImage"><img class="menuTopImage" src="images/menu/MainCourses.jpg"></div>
    </div>
    <div class="col-md-12 leftDiv drinks">
        <div class="menuBackgroundDiv leftDiv" id="drinksImage"><img class="menuTopImage" src="images/menu/Drinks.jpg"></div>
    </div>
    <div class="col-md-12 leftDiv desserts">
        <div class="menuBackgroundDiv leftDiv" id="dessertImage"><img class="menuTopImage" src="images/menu/Desserts.jpg"></div>
    </div>

    <div class="container">
        <div class="row">
            <div id="myBtnContainer">
                <button class="btn menuBtn active" onclick="filterSelection('starters')" id="menuAppertizer"> AntiPasta</button>
                <button class="btn menuBtn" onclick="filterSelection('salads')"> Sallata</button>
                <button class="btn menuBtn" onclick="filterSelection('mainCourses')" id="mainCourses"> Pjata Kryesore</button>
                <button class="btn menuBtn" onclick="filterSelection('drinks')" id="menuDrinks"> Pije</button>
                <button class="btn menuBtn" onclick="filterSelection('desserts')" id="menuDesserts">Deserte</button>
            </div>
            <div class="container">
                <div class="row menuContent">
                    <div class="col-md-6 leftDiv starters">
                        <h1 class="menuLeftTitle">Antipasta</h1>
                        <div class="FoodItem">
                            <p class="menuFoodTitle">Mozzarella e fërguar &nbsp;&nbsp;|&nbsp;&nbsp;3,00€</p>
                            <p class="menuFoodDesc">(1 porcion - 3 copa)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Toptha nga patatja &nbsp;&nbsp;|&nbsp;&nbsp;3,50€</p>
                            <p class="menuFoodDesc">(1 porcion - 5 copa)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Djath i fërguar në furrë&nbsp;&nbsp;|&nbsp;&nbsp;3,50€</p>
                            <p class="menuFoodDesc">(1 porcion)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Pogaçe shtëpie&nbsp;&nbsp;|&nbsp;&nbsp;3,50€</p>
                            <p class="menuFoodDesc">(1 porcion)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Paragjellë e ftohtë - Ordever për 1 person&nbsp;&nbsp;|&nbsp;&nbsp;6,00€</p>
                            <p class="menuFoodDesc">(Përshutë, kaçkavall, djath shtëpie, sallatë franceze, ullinj, salcë)</p>
                        </div>
                    </div>
                    <div class="col-md-6 leftDiv starters" id="rightDiv">
                        <h1 class="menuLeftTitle">Supa</h1>
                        <div class="FoodItem">
                            <p class="menuFoodTitle">Crem Supë Brokoli mbretërore Maélys&nbsp;&nbsp;|&nbsp;&nbsp;3,00€</p>
                            <p class="menuFoodDesc">(bouillon, brokoli, mëlmesa, ajkë, arra, djath)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Crem Supë Karote&nbsp;&nbsp;|&nbsp;&nbsp;2,50€</p>
                            <p class="menuFoodDesc">(Bouillon, karrotë, mëlmesa, ajkë, kubëza buke)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Crem Supë Domate&nbsp;&nbsp;|&nbsp;&nbsp;2,50€</p>
                            <p class="menuFoodDesc">(Bouillon, domate, qepë, hudhër, ajkë, mëlmesa)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Çorbë Viçi&nbsp;&nbsp;|&nbsp;&nbsp;3,00€</p>
                            <p class="menuFoodDesc">(bulon viçi, mish viçi, perime, mëlmesa)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Çorbë Pule&nbsp;&nbsp;|&nbsp;&nbsp;2,50€</p>
                            <p class="menuFoodDesc">(bulon, mish pule, perime, mëlmesa, majdanoz)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Çorbë Peshku&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>
                            <p class="menuFoodDesc">(bulon, peshk, salcë domatesh, mëlmesa, hudhër, perime)</p>
                        </div>

                    </div>
                    <div class="col-md-6 leftDiv salads">
                        <h1 class="menuLeftTitle">Sallata</h1>
                        <div class="FoodItem">
                            <p class="menuFoodTitle">Sallatë mbretërore Maélys&nbsp;&nbsp;|&nbsp;&nbsp;4,50€</p>
                            <p class="menuFoodDesc">(Sallatë, rukola, iceberg, radicchio, finokio, tomatini, portokall, salcë dresing)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Sallatë Mix&nbsp;&nbsp;|&nbsp;&nbsp;3,50€</p>
                            <p class="menuFoodDesc">(Sallatë, tomatini, tranguj, speca tricolore, ullinj, mozzarella, salcë dresing, misër, qepë e kuqe)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Sallatë Cezar&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>
                            <p class="menuFoodDesc">(Sallatë, steak pule, parmezan, domate e tharë (cherry), limon, mëlmesa, kubëza buke, dresing)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Sallatë Mozzarella&nbsp;&nbsp;|&nbsp;&nbsp;3,50€</p>
                            <p class="menuFoodDesc">(Djath Mozzarella, domate të freskëta, salcë ulliri)</p>
                        </div>
                    </div>
                    <div class="col-md-6 leftDiv salads" id="rightDiv">
                        <h1 class="menuLeftTitle">Sallata</h1>
                        <div class="FoodItem">
                            <p class="menuFoodTitle">Sallatë Rukola&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>
                            <p class="menuFoodDesc">(Sallatë rukola, salcë hudhre, qepë, majdanoz, badem, borzilok, mëlmesa, parmezan, dresing)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Sallatë Jeshile&nbsp;&nbsp;|&nbsp;&nbsp;2,50€</p>
                            <p class="menuFoodDesc">(Sallatë jeshile, mëlmesa, salcë dresing)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Sallatë Tuna&nbsp;&nbsp;|&nbsp;&nbsp;3,80€</p>
                            <p class="menuFoodDesc">(Sallatë Iceberg, radicchio, tomatini, tuna, qepë, salcë dresing)</p>
                        </div>

                    </div>
                    <div class="col-md-6 leftDiv mainCourses">
                        <h1 class="menuLeftTitle">Pasta</h1>
                        <div class="FoodItem">
                            <p class="menuFoodTitle">Pappardelle mbretërore Maélys&nbsp;&nbsp;|&nbsp;&nbsp;5,00€</p>
                            <p class="menuFoodDesc">(Pappardelle, biftek (file viçi), perime të freskëta, salcë domatesh, mëlmesa të ndryshme, parmezan)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Pappardelle me mish pule & Brokoli&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>
                            <p class="menuFoodDesc">(Pappardelle, steak pule, brokoli, salcë borziloku (pesto genovese), mëlmesa të ndryshme, parmezan)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Tagliatelle Viçi&nbsp;&nbsp;|&nbsp;&nbsp;4,50€</p>
                            <p class="menuFoodDesc">(Tagliatelle, mish viçi, salcë spinaqi, mëlmesa të ndryshme, parmezan)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Tagliatelle vegjetariane me salcë tartufi&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>
                            <p class="menuFoodDesc">(Tagliatelle, perime të freskëta, salcë tartufi, mëlmesa të ndryshme, parmezan)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Penne ose Spaghetti Bolognese&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>
                            <p class="menuFoodDesc">(Penne, salcë boloneze, mëlmesa të ndryshme, parmezan)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Penne ose Spaghetti Carbonara&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>
                            <p class="menuFoodDesc">(Penne, përshutë, kërpudha, salcë, parmezan)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Penne ose Spinach Spaghetti&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>
                            <p class="menuFoodDesc">(Penne, steak pule, salcë spinaqi, perime të freskëta, parmezan)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Penne ose Spaghetti Napoli&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>
                            <p class="menuFoodDesc">(Penne, salcë domatesh, vaj ulliri, parmezan)</p>
                        </div>
                    </div>
                    <div class="col-md-6 leftDiv mainCourses" id="rightDiv">
                        <h1 class="menuLeftTitle">Pizza</h1>
                        <div class="FoodItem">
                            <p class="menuFoodTitle">Pizza mbretërore Maélys&nbsp;&nbsp;|&nbsp;&nbsp;8,00€</p>
                            <p class="menuFoodDesc">(Salcë domatesh, mozzarella e grirë, salmon i tymosur, ullinj, kungulleshkë, domate, hudhër, asparagus, origano)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Pizza Quattro Formaggi (Pizza 4 djathëra)&nbsp;&nbsp;|&nbsp;&nbsp;6,00€</p>
                            <p class="menuFoodDesc">(Salcë domatesh, mozzarella e grirë, 4 lloj djathërash, ullinj, origano)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Pizza Gorgonzola&nbsp;&nbsp;|&nbsp;&nbsp;6,00€</p>
                            <p class="menuFoodDesc">(Salcë domatesh, mozzarella e grirë, kërpudha, djath gorgonzola, origano)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Pizza me fruta deti&nbsp;&nbsp;|&nbsp;&nbsp;10,00€</p>
                            <p class="menuFoodDesc">(Salcë domatesh, mozzarella e grirë, fruta deti, speca tricolore, hudhër, origano, mëlmesa, domate, rukola)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Pizza Tuna&nbsp;&nbsp;|&nbsp;&nbsp;5,50€</p>
                            <p class="menuFoodDesc">(Salcë domatesh, mozzarella e grirë, tuna, qepë, mëlmesa, origano)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Pizza Mozzarella&nbsp;&nbsp;|&nbsp;&nbsp;6,00€</p>
                            <p class="menuFoodDesc">(Salcë domatesh, mozzarella e grirë, mozzarella, domate, borzilok i freskët, mëlmesa, origano, hudhër)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Pizza Meksikane&nbsp;&nbsp;|&nbsp;&nbsp;6,50€</p>
                            <p class="menuFoodDesc">(Salcë domatesh, mozzarella e grirë, suxhuk shtëpie, spec djegës, mëlmesa, origano)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Pizza Përshutë&nbsp;&nbsp;|&nbsp;&nbsp;4,50€</p>
                            <p class="menuFoodDesc">(Përshutë, salcë domatesh, mozzarella e grirë, mëlmesa, origano)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Pizza Margherita&nbsp;&nbsp;|&nbsp;&nbsp;4,50€</p>
                            <p class="menuFoodDesc">(Salcë domatesh, mozzarella e grirë, mëlmesa, origano)</p>
                        </div>

                    </div>
                    <div class="col-md-6 leftDiv mainCourses">
                        <h1 class="menuLeftTitle">Biftek</h1>
                        <div class="FoodItem">
                            <p class="menuFoodTitle">Biftek mbretëror Maélys&nbsp;&nbsp;|&nbsp;&nbsp;13,50€</p>
                            <p class="menuFoodDesc">(Biftek, gorgonzola, perime të freskëta, mozzarella, mëlmesa, salcë mishi (kërpudha), pure patatesh, karota)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Biftek Pepe Verde&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>
                            <p class="menuFoodDesc">(Biftek, perime të freskëta, mëlmesa, salcë mishi Pepe Verde, pure patatesh, brokoli)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Biftek Natyral&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>
                            <p class="menuFoodDesc">(Biftek, patate të fërguara, oriz, mëlmesa)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Biftek Mozzarella&nbsp;&nbsp;|&nbsp;&nbsp;12,50€</p>
                            <p class="menuFoodDesc">(Biftek, mozzarella, pure patatesh, bizele, karotë, mëlmesa, salcë mishi)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Biftek me salcë tartufi&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>
                            <p class="menuFoodDesc">(Biftek, pure patatesh, bizele, spinaq, oriz, salcë tartufi, mëlmesa)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Biftek me kërpudha të freskëta&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>
                            <p class="menuFoodDesc">(Biftek, kërpudha të freskëta, perime të freskëta, mëlmesa, salcë mishi)</p>
                        </div>
                    </div>
                    <div class="col-md-6 leftDiv mainCourses" id="rightDiv">
                        <h1 class="menuLeftTitle">Taraker</h1>
                        <div class="FoodItem">
                            <p class="menuFoodTitle">Taraker mbretëror Maélys&nbsp;&nbsp;|&nbsp;&nbsp;11,00€</p>
                            <p class="menuFoodDesc">(Taraker, gorgonzola, perime të freskëta, mozzarella, mëlmesa, salcë mishi (kërpudha), pure patatesh, karota)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Taraker Pepe Verde&nbsp;&nbsp;|&nbsp;&nbsp;9,50€</p>
                            <p class="menuFoodDesc">(Taraker, perime të freskëta, mëlmesa, salcë mishi Pepe Verde, pure patatesh, brokoli)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Taraker Natyral&nbsp;&nbsp;|&nbsp;&nbsp;9,50€</p>
                            <p class="menuFoodDesc">(Taraker, patate të fërguara, oriz, mëlmesa)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Taraker Mozzarella&nbsp;&nbsp;|&nbsp;&nbsp;10,00€</p>
                            <p class="menuFoodDesc">(Taraker, mozzarella, pure patatesh, bizele, karotë, mëlmesa, salcë mishi)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Taraker me salcë tartufi &nbsp;&nbsp;|&nbsp;&nbsp;9,50€</p>
                            <p class="menuFoodDesc">(Taraker, pure patatesh, bizele, spinaq, oriz, salcë tartufi, mëlmesa)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Taraker me kërpudha të freskëta&nbsp;&nbsp;|&nbsp;&nbsp;9,50€</p>
                            <p class="menuFoodDesc">(Taraker, kërpudha të freskëta, perime të freskëta, mëlmesa, salcë mishi)</p>
                        </div>

                    </div>
                    <div class="col-md-6 leftDiv mainCourses">
                        <h1 class="menuLeftTitle">Ramstek</h1>
                        <div class="FoodItem">
                            <p class="menuFoodTitle">Ramstek mbretëror Maélys&nbsp;&nbsp;|&nbsp;&nbsp;10,00€</p>
                            <p class="menuFoodDesc">(Ramstek, gorgonzola, perime të freskëta, mozzarella, mëlmesa, salcë mishi (kërpudha), pure patatesh, karota)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Ramstek Pepe Verde&nbsp;&nbsp;|&nbsp;&nbsp;9,00€</p>
                            <p class="menuFoodDesc">(Ramstek, perime të freskëta, mëlmesa, salcë mishi Pepe Verde, pure patatesh, brokoli)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Ramstek Natyral&nbsp;&nbsp;|&nbsp;&nbsp;9,00€</p>
                            <p class="menuFoodDesc">(Ramstek, patate të fërguara, oriz, mëlmesa)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Ramstek Mozzarella&nbsp;&nbsp;|&nbsp;&nbsp;9,50€</p>
                            <p class="menuFoodDesc">(Ramstek, mozzarella, pure patatesh, bizele, karotë, mëlmesa, salcë mishi)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Ramstek me salcë tartufi&nbsp;&nbsp;|&nbsp;&nbsp;9,00€</p>
                            <p class="menuFoodDesc">(Ramstek, pure patatesh, bizele, spinaq, oriz, salcë tartufi, mëlmesa)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Ramstek me kërpudha të freskëta&nbsp;&nbsp;|&nbsp;&nbsp;9,00€</p>
                            <p class="menuFoodDesc">(Ramstek, kërpudha të freskëta, perime të freskëta, mëlmesa, salcë mishi)</p>
                        </div>
                    </div>
                    <div class="col-md-6 leftDiv mainCourses" id="rightDiv">
                        <h1 class="menuLeftTitle">Specialitete pule</h1>
                        <div class="FoodItem">
                            <p class="menuFoodTitle">Steak pule mbretëror Maélys&nbsp;&nbsp;|&nbsp;&nbsp;8,00€</p>
                            <p class="menuFoodDesc">(Steak pule, salcë pesto genovese, paparedelle, pure patatesh, karota, brokoli, mëlmesa)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Steak pule Vieneze&nbsp;&nbsp;|&nbsp;&nbsp;7,00€</p>
                            <p class="menuFoodDesc">(Steak pule spinaq, mozzarella, patate të fërguara, salcë kuzhine, mëlmesa)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Rollatë pule&nbsp;&nbsp;|&nbsp;&nbsp;7,00€</p>
                            <p class="menuFoodDesc">(Steak pule, spinaq, mozzarella e grirë, patate të fërguara, salcë kuzhine, perime të freskëta)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond" style="visibility: hidden;">
                            <p class="menuFoodTitle">Crem Soup Brokoli mbreterore Maelys</p>
                            <p class="menuFoodDesc">Crem Soup Brokoli mbreterore Maelys</p>
                        </div>
                        <div class="FoodItem FoodItemSecond" style="visibility: hidden;">
                            <p class="menuFoodTitle">Crem Soup Brokoli mbreterore Maelys</p>
                            <p class="menuFoodDesc">Crem Soup Brokoli mbreterore Maelys</p>
                        </div>
                        <div class="FoodItem FoodItemSecond" style="visibility: hidden;">
                            <p class="menuFoodTitle ">Crem Soup Brokoli mbreterore Maelys</p>
                            <p class="menuFoodDesc">Crem Soup Brokoli mbreterore Maelys</p>
                        </div>
                        <div class="FoodItem FoodItemSecond" style="visibility: hidden;">
                            <p class="menuFoodTitle ">Crem Soup Brokoli mbreterore Maelys</p>
                            <p class="menuFoodDesc">Crem Soup Brokoli mbreterore Maelys</p>
                        </div>

                    </div>
                    <div class="col-md-6 leftDiv mainCourses">
                        <h1 class="menuLeftTitle">Rizoto</h1>
                        <div class="FoodItem">
                            <p class="menuFoodTitle">Rizoto mbretëror Maélys&nbsp;&nbsp;|&nbsp;&nbsp;4,50€</p>
                            <p class="menuFoodDesc">(Oriz basmat, perime të freskëta, file viçi, salcë tartufi)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Rizoto me gjalpë&nbsp;&nbsp;|&nbsp;&nbsp;3,50€</p>
                            <p class="menuFoodDesc">(Oriz basmat, gjalpë, mëlmesa të ndryshme, parmezan)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Rizoto vegjetarian&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>
                            <p class="menuFoodDesc">(Oriz basmat, perime të freskëta, salcë, parmezan)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Rizoto spinaqi&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>
                            <p class="menuFoodDesc">(Oriz basmat, salcë spinaqi, gjalpë, steak pule, mëlmesa të ndryshme, parmezan)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Rizoto pule&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>
                            <p class="menuFoodDesc">(Oriz basmat, steak pule, perime të freskëta, salcë curry, mëlmesa të ndryshme, parmezan)</p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Rizoto fruta deti&nbsp;&nbsp;|&nbsp;&nbsp;7,00€</p>
                            <p class="menuFoodDesc">(Oriz basmat, fruta deti, salcë domatesh, perime të freskëta, tomatini)</p>
                        </div>
                    </div>
                    <div class="col-md-6 leftDiv mainCourses" id="rightDiv">
                        <h1 class="menuLeftTitle">Ushqim Deti</h1>
                        <div class="FoodItem">
                            <p class="menuFoodTitle">Karkaleca mbretëror Maélys&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Karkaleca në skarë&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Kallamari i fërguar&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Kallamari të mbushur&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Levrek&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Koc&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Salmon&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Troftë&nbsp;&nbsp;|&nbsp;&nbsp;6,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>

                    </div>
                    <div class="col-md-6 leftDiv drinks">
                        <h1 class="menuLeftTitle">Drinks</h1>
                        <div class="FoodItem">
                            <p class="menuFoodTitle">Schwarzwälder Kirschwasser&nbsp;&nbsp;|&nbsp;&nbsp;2,20€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Williams-Christ-Birne&nbsp;&nbsp;|&nbsp;&nbsp;2,50€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Wald Himbeergeist&nbsp;&nbsp;|&nbsp;&nbsp;2,50€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Mirabell&nbsp;&nbsp;|&nbsp;&nbsp;2,20€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Schwarzwälder Zwetschgenwasser&nbsp;&nbsp;|&nbsp;&nbsp;2,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Ingwer Likör&nbsp;&nbsp;|&nbsp;&nbsp;2,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Gin&nbsp;&nbsp;|&nbsp;&nbsp;1,50€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">George Rum&nbsp;&nbsp;|&nbsp;&nbsp;1,50€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Verak&nbsp;&nbsp;|&nbsp;&nbsp;2,50€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Kruškovac Birne&nbsp;&nbsp;|&nbsp;&nbsp;4,50€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                    </div>
                    <div class="col-md-6 leftDiv drinks" id="rightDiv">
                        <h1 class="menuLeftTitle">Pije</h1>
                        <div class="FoodItem">
                            <p class="menuFoodTitle ">Cassis Schwarzer Johanisbeer Likör&nbsp;&nbsp;|&nbsp;&nbsp;3,50€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Schwarzwälder Kirschenlikör&nbsp;&nbsp;|&nbsp;&nbsp;4,90€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Schwarzwälder Pfirischlikör&nbsp;&nbsp;|&nbsp;&nbsp;4,90€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Schoko-Kirsch Sahnelikör&nbsp;&nbsp;|&nbsp;&nbsp;5,90€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Schwarzwald Teufel&nbsp;&nbsp;|&nbsp;&nbsp;6,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Caipirinha Samira&nbsp;&nbsp;|&nbsp;&nbsp;4,90€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Pina Colada<&nbsp;&nbsp;|&nbsp;&nbsp;4,90€/p> <p class="menuFoodDesc">
                            </p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Mojito&nbsp;&nbsp;|&nbsp;&nbsp;4,90€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Brandy Likör&nbsp;&nbsp;|&nbsp;&nbsp;4,50€</p>
                            <p class="menuFoodDesc"></p>
                        </div>

                    </div>
                    <div class="col-md-6 leftDiv desserts">
                        <h1 class="menuLeftTitle">Deserte</h1>
                        <div class="FoodItem">
                            <p class="menuFoodTitle">Souffle Çokollatë&nbsp;&nbsp;|&nbsp;&nbsp;3,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Panna Cotta&nbsp;&nbsp;|&nbsp;&nbsp;2,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Tiramisu&nbsp;&nbsp;|&nbsp;&nbsp;2,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Kadaif&nbsp;&nbsp;|&nbsp;&nbsp;2,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Crêpe me Nutella (3 copa)&nbsp;&nbsp;|&nbsp;&nbsp;3,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Crêpe me mjaltë (3 copa)&nbsp;&nbsp;|&nbsp;&nbsp;3,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Crêpe me fruta (3 copa)&nbsp;&nbsp;|&nbsp;&nbsp;3,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Bakllavë me kadaif&nbsp;&nbsp;|&nbsp;&nbsp;2,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Bakllavë me akullore&nbsp;&nbsp;|&nbsp;&nbsp;3,50€</p>
                            <p class="menuFoodDesc"></p>
                        </div>

                    </div>
                    <div class="col-md-6 leftDiv desserts" id="rightDiv">
                        <h1 class="menuLeftTitle">Akullore</h1>
                        <div class="FoodItem">
                            <p class="menuFoodTitle">lajthi&nbsp;&nbsp;|&nbsp;&nbsp;1,80€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Vanillë&nbsp;&nbsp;|&nbsp;&nbsp;1,80€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Stracciatella&nbsp;&nbsp;|&nbsp;&nbsp;1,80€ </p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Çokollatë&nbsp;&nbsp;|&nbsp;&nbsp;1,80€ </p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Luleshtrydhe&nbsp;&nbsp;|&nbsp;&nbsp;1,80€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Karamel&nbsp;&nbsp;|&nbsp;&nbsp;1,80€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Banane&nbsp;&nbsp;|&nbsp;&nbsp;1,80€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Snickers&nbsp;&nbsp;|&nbsp;&nbsp;1,80€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Fruta mali&nbsp;&nbsp;|&nbsp;&nbsp;1,80€</p>
                            <p class="menuFoodDesc"></p>
                        </div>

                    </div>

                </div>
            </div>

            <script>
                filterSelection("starters")

                function filterSelection(c) {
                    var x, i;
                    x = document.getElementsByClassName("leftDiv");
                    if (c == "starters");
                    for (i = 0; i < x.length; i++) {
                        w3RemoveClass(x[i], "show");
                        if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
                    }
                }

                function w3AddClass(element, name) {
                    var i, arr1, arr2;
                    arr1 = element.className.split(" ");
                    arr2 = name.split(" ");
                    for (i = 0; i < arr2.length; i++) {
                        if (arr1.indexOf(arr2[i]) == -1) {
                            element.className += " " + arr2[i];
                        }

                    }
                }

                function w3RemoveClass(element, name) {
                    var i, arr1, arr2;
                    arr1 = element.className.split(" ");
                    arr2 = name.split(" ");
                    for (i = 0; i < arr2.length; i++) {
                        while (arr1.indexOf(arr2[i]) > -1) {
                            arr1.splice(arr1.indexOf(arr2[i]), 1);
                        }
                    }
                    element.className = arr1.join(" ");
                }

                // Add active class to the current button (highlight it)
                var btnContainer = document.getElementById("myBtnContainer");
                var btns = btnContainer.getElementsByClassName("btn");
                for (var i = 0; i < btns.length; i++) {
                    btns[i].addEventListener("click", function() {
                        var current = document.getElementsByClassName("active ");
                        current[0].className = current[0].className.replace(" active", "");
                        $('button.btn.menuBtn').removeClass('active');
                        this.className += " active";

                    });

                }
                $(function() {
                    menuFunction();
                    $(window).bind("hashchange", menuFunction());

                });

                function menuFunction() {
                    if (window.location.hash == "#drinksImage") {
                        filterSelection('drinks')
                        $("#menuDrinks").addClass('active');
                        $("#menuAppertizer").removeClass('active');
                    } else if (window.location.hash == "#coursesImage") {
                        filterSelection('mainCourses')
                        $("#mainCourses").addClass('active');
                        $("#menuDrinks").removeClass('active');
                        $("#menuAppertizer").removeClass('active');

                    } else if (window.location.hash == "#dessertImage") {
                        filterSelection('desserts')
                        $("#menuDesserts").addClass('active');
                        $("#mainCourses").removeClass('active');
                        $("#menuDrinks").removeClass('active');
                        $("#menuAppertizer").removeClass('active');


                    } else {
                        $("#menuAppertizer").addClass('active');

                    }
                }
            </script>
            <div class="col-md-10 col-md-offset-1">
            </div>
            <!--row-->
        </div>
    </div>
        <!--container-->
</section>
<!-- /MAIN WRAP CONTENT -->

<?php
include "include/footer.php";
?>