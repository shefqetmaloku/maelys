<?php

include("include/header.php");

?>

<style>

   @import url('https://fonts.googleapis.com/css?family=Playfair+Display&display=swap');

</style>



<!-- HOME SLIDER -->

<!-- Go to js/init.js line 17 to add slider images -->

<div class="home-slider">

   <div class="intro-text container">

      <h1 class="slider-slogan">Experience the real</h1>

      <span class="luxury">LUXURY</span>

      <span class="stars"><img class="singleStar firstStar" src="../images/home/Star.svg"><img class="singleStar" src="../images/home/Star.svg"><img class="singleStar" src="../images/home/Star.svg"><img class="singleStar" src="../images/home/Star.svg"><img class="singleStar lastStar" src="../images/home/Star.svg"></span>



      <!-- <a href="reservation.php" class="view-more more-white">Book a Table</a> -->

   </div>

   <div class="scrollDown">

      <a href="#home-content-1" class="scrollDownLink"><span class="arrowDown"></span></a>

   </div>

</div>

<!-- /HOME SLIDER -->

<section id="home-content-1" class="home-widget home-features">

   <div class="container welcomeContainer">

      <h3 class="welcomeTitle">Mirësevini në<br class="breakResponsive"> Maélys Restaurant</h3>

      <p class="welcomeContent">Restauranti jonë ofron një gamë të gjërë të ushqimeve të shijshme, të përgaditura me shumë dashuri dhe pasion nga kuzhinieri më i mirë në qytet. Të gjithë përbërësit janë të zgjedhur me shumë kujdes që të kenë kualitetin dhe freskinë më të lartë.</p>

      <span class="stars"><img class="singleStar firstStar" src="images/home/Star.svg"><img class="singleStar" src="images/home/Star.svg"><img class="singleStar" src="images/home/Star.svg"><img class="singleStar" src="images/home/Star.svg"><img class="singleStar lastStar" src="images/home/Star.svg"></span>

   </div>

   <!--.container-->

</section>

<!-- SECTION 2 -->

<section id="home-content-2" class="home-widget">

   <div class="container aboutUsContainer">

      <div class="row">



         <div class="col-md-6">

            <!-- <h2 class="home-subtitle">History</h2> -->

            <div class="centerMobile">

               <h1 class="home-title margin-b24 title-headline">Rreth Nesh</h1>

            </div>

            <!-- <div class="title-style-1">Welcome to Maelys, a modern restaurant with a focus on premium food tastes</div> -->

            <div class="aboutUsSpan">

               <p class="aboutUsParagraph">I vendosur në qendër të qytetit të Deçanit, Maeélys Restaurant & Irish Bar ofron një pamje mbresëlënëse të bjeshkëve të famshme shqiptare. Duke u nisur nga enterieri mbretëror deri te pijet dhe ushqimet e mrekullueshme, ambienti i restaurantit tonë është po aq tërheqës sa kuzhina jonë.

                  <br><br>



                 Ne ofrojmë një koncept unik të ushqimit, duke përfshirë antipastat më të mira, peshkun më të freskët, ushqimet e mahnitshme të detit dhe biftekun e jashtëzakonshëm. Një detaj tjetër unik i restaurantit tonë janë pijet dhe likerët e importuara nga Gjermania, karakteristikë e cila na dallon nga restaurantet tjera.

               </p>

            </div>

         </div>



      </div>

      <!-- /row-->

   </div>

   <!-- /container-->

</section>

<!-- /SECTION 2 -->



<!-- SECTION 3 -->

<!-- Add parallax class to a section and a background image to create the parallax effect -->

<section class="home-widget parallax" style="background-image:url('images/home/about-4.jpg');">

   <div class="parallax-content">

      <div class="container">

         <div class="row">



            <div class="col-md-12">

               <div class="img-center alignc">

                  <img class="img-responsive img-feature foodSlogan" src="images/home/FoodSloganshqip-02.svg" alt="taste-food" />

               </div>

            </div>



         </div>

         <!--.row-->

      </div>

      <!--.container-->

   </div>

   <!-- /parallax-content -->

</section>

<!-- /SECTION 3 -->



<!-- SECTION 4 -->

<section id="home-content-4" class="home-widget">

   <div class="container">

      <div class="row">



         <div class="col-md-12 alignc">

            <!-- <h2 class="home-subtitle">Discover</h2> -->

            <h1 class="home-title margin-b24 title-headline menuH1">Menu</h1>

            <p class="menuParagraph">Menuja jonë vë në pah fokusin tonë unik në përbërësit e ushqimit, qoftë ai bifteku i jashtëzakonshëm i zgjedhur nga kasapi ynë apo nga ushqimi i freskët i detit, çdo përbërës transformohet në diçka tepër të shijshme. Shijojeni!</p>

            <div class="MenuCategory">

               <div class="row menuRow">

                  <div class="col-md-4 menuCol firstMenuCol">

                     <div class="hoverImage">

                        <a href="menu.php#coursesImage" class="toMenu"><img src="images/home/FoodImage.jpg"></a>

                     </div>

                     <h3 class="menuTitle">Pjatat kryesore</h3>



                  </div>

                  <div class="col-md-4 menuCol secondMenuCol">

                     <div class="hoverImage">

                        <a href="menu.php#drinksImage" class="toMenu" id="indexDrinks"><img src="images/home/DrinksImage.jpg"></a>

                     </div>

                     <h3 class="menuTitle">Pijet</h3>



                  </div>

                  <div class="col-md-4 menuCol thirdMenuCol">

                     <div class="hoverImage">

                        <a href="menu.php#dessertImage" class="toMenu"><img src="images/home/DessertsImage.jpg"></a>

                     </div>

                     <h3 class="menuTitle">Desertet</h3>

                  </div>

               </div>

               <!-- FOOD MENU -->

               <ul class="our-menu">

               </ul>

               <!-- /FOOD MENU-->



               <a href="menu.php" class="view-more margin-t36 goToMenu" id="goToMenu">MENU</a>



            </div>

            <!--.col-md-12-->

         </div>

         <!--.row-->

      </div>

      <!--.container-->

</section>

<!-- /SECTION 4 -->



<!-- SECTION 5 -->

<!-- Add parallax class to a section and a background image to create the parallax effect -->

<section class="home-widget parallax" style="background-image:url('images/home/parallax-2.jpg');">

   <div class=backgroundOverlay></div>

   <div class="parallax-content">

      <div class="container">

         <div class="row">

            <img class="luxDesignImg" src="images/home/Dizajnenterieri-01.svg" />

         </div>

         <!--.row-->

      </div>

      <!--.container-->

   </div>

   <!-- parallax-content -->

</section>

<!-- /SECTION 5 -->

<!-- SECTION 2 -->

<section id="home-content-6" class="home-widget">

   <div class="container aboutUsContainer">

      <div class="row WinesRow">



         <div class="col-md-6 winesDiv">

            <!-- <h2 class="home-subtitle">History</h2> -->

            <div class="centerMobile">

               <h1 class="home-title margin-b24 title-headline winesH1">Pijet Unike</h1>

            </div>

            <div class="WinesSpan">

               <p class="WinesParagraph">Ne ju ofrojmë pijet më unike, veçanërisht një listë të pijeve alkoolike me shije origjinale të importuara ekskluzivisht nga Gjermania dhe të servuara në mënyrë luksoze në Restaurantin dhe Irish barin tonë. Ne, ju ftojmë që të vini dhe të shijoni pijet tona mbresëlënëse të cilat nuk mund t’i shijoni ndokund tjetër.

               </p>

            </div>

         </div>

      </div>

      <!-- /row-->

   </div>

   <!-- /container-->

</section>

<!-- /SECTION 2 -->



<section id="home-content-end" class="home-widget home-features">

   <div class="container welcomeContainer endContainer">

      <h3 class="welcomeTitle quotes">“</h3>

      <!-- <h3 class="welcomeTitle">Welcome to Maélys Restaurant</h3> -->

      <p class="welcomeContent quoteContent">Gjithçka në këtë jetë është të përjetosh çaste të bukura me më të dashurit tuaj!</p>

      <img class="ceoSignature endPage" src="images/home/ceoSignature.svg">



   </div>

   <!--.container-->

</section>



<?php

include("include/footer.php");

?>