<?php
include("include/headerspec.php");
?>

<!-- TOP IMAGE HEADER -->
<section class="topSingleBkg topPageBkg impressum">
    <div class="item-content-bkg">
        <div class="item-img"></div>
    </div>
</section>
<!-- /TOP IMAGE HEADER -->

<!-- MAIN WRAP CONTENT -->
<section id="wrap-content" class="page-content bookContent impressumContent">
    <div class="container impressumContainer">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <h1 class="impressumTitle">Impressum</h1>
                <h3 class="impressumPoint">Privacy Policy</h3>
                <p>Politika e privatësisë
                    Operatorët e këtyre faqeve e konsiderojnë shumë seriozisht mbrojtjen e të dhënave tuaja personale. Ne i trajtojmë të dhënat tuaja personale në mënyrë konfidenciale dhe në përputhje me rregulloret ligjore për mbrojtjen e të dhënave dhe këtë politikë të privatësisë.
                    Përdorimi i faqes sonë të internetit zakonisht është i mundur pa siguruar informacion personal.
                    Sa i përket anëve tona, të dhënat personale (për shembull emri, adresa ose adresat E-Mail) janë ngritur, kjo bëhet, aq sa është e mundur, gjithnjë në bazë të freiwilliger. Këto të dhëna nuk do t'u zbulohen palëve të treta pa pëlqimin tuaj të qartë.
                    Ju lutemi vini re se transmetimi i të dhënave përmes Internetit (për shembull, kur komunikoni me postë elektronike) mund të ketë dobësi sigurie.
                    Një mbrojtje e plotë e të dhënave nga qasja nga palët e treta nuk është e mundur.
                </p>
                <h3 class="impressumPoint">Biskota</h3>
                <p>Faqet e internetit përdorin pjesërisht të ashtuquajturat cookie. Cookies nuk dëmtojnë kompjuterin tuaj dhe nuk përmbajnë viruse. Cookies përdoren për ta bërë ofertën tonë më miqësore, efektive dhe të sigurt. Cookies janë skedarë të vegjël teksti që ruhen në kompjuterin tuaj dhe ruhen nga shfletuesi juaj.
                    Shumica e cookies që përdorim janë të ashtuquajturat cookie-s për sesione. Ata do të fshihen automatikisht në fund të vizitës suaj. Cookie të tjera mbeten të ruajtura në pajisjen tuaj derisa t'i fshini ato. Këto cookie na lejojnë të njohim shfletuesin tuaj në vizitën tuaj të ardhshme.
                    Ju mund të vendosni shfletuesin tuaj në mënyrë që të informoheni për vendosjen e cookies dhe të lejoni cookies vetëm në raste individuale, pranimin e cookies për raste specifike ose në përgjithësi
                    përjashtoni dhe aktivizoni fshirjen automatike të cookies kur mbyllni shfletuesin. Disaktivizimi i cookies mund të kufizojë funksionalitetin e kësaj faqe në internet.

                </p>

                <h3 class="impressumPoint">Fotografi server log</h3>
                <p>Ofruesi i faqeve automatikisht mbledh dhe ruan informacione në të ashtuquajturat skedarë log të serverëve, të cilët shfletuesi juaj automatikisht na transmeton. Këto janë:
                    <ul>
                        <li class="impressumBullet">lloji i shfletuesit dhe versioni i shfletuesit</li>
                        <li class="impressumBullet">sistemi operativ i përdorur</li>
                        <li class="impressumBullet">URL e referuesit</li>
                        <li class="impressumBullet">Emri pritës i kompjuterit hyrës</li>
                        <li class="impressumBullet">Koha e kërkesës së serverit</li><br>
                    </ul>
                    Këto të dhëna nuk mund t'u shpërndahen personave specifikë. Një shkrirje e këtyre të dhënave me
                    burime të tjera të të dhënave nuk do të bëhen. Ne rezervojmë të drejtën t'i shtojmë këto të dhëna më vonë
                    kontrolloni nëse jemi në dijeni të indikacioneve konkrete për përdorim të paligjshëm.

                </p>

                <h3 class="impressumPoint">Kontakt</h3>
                <p>Nëse na dërgoni pyetje përmes formularit të kontaktit, detajet tuaja do të merren nga
                    Formulari i hetimit përfshirë detajet e kontaktit që keni dhënë atje me qëllim të përpunimit të
                    Kërkoni dhe në rast të pyetjeve vijuese të ruajtura me ne. Ne nuk do ta ndajmë këtë informacion pa pëlqimin tuaj.

                </p>

                <h3 class="impressumPoint">Shtojca e shfletuesit</h3>
                <p>Ju mund të parandaloni ruajtjen e cookies me një përcaktim përkatës të softverit tuaj të shfletuesit; megjithatë, ne theksojmë se në këtë rast ju mund të mos jeni në gjendje të përdorni të gjitha funksionet e kësaj faqe në internet. Përveç kësaj, ju mund të parandaloni mbledhjen e të dhënave të krijuara nga cookie dhe që lidhen me përdorimin tuaj të faqes në internet (përfshirë adresën tuaj IP) në Google dhe përpunimin e këtyre të dhënave nga Google duke shkarkuar shtojcën e shfletuesit të mundshme nën lidhjen e mëposhtme dhe instaloni:
                    <strong class="linku">https://tools.google.com/dlpage/gaoptout?hl=de</strong><br>
                    Kundërshtimi i mbledhjes së të dhënave<br>
                    Ju mund të parandaloni mbledhjen e të dhënave tuaja nga Google Analytics duke klikuar në lidhjen e mëposhtme. Do të vendoset një cookie zgjedhëse për të parandaluar mbledhjen e të dhënave tuaja gjatë vizitave të ardhshme në këtë sit: çaktivizoni Google Analytics
                    Ju mund të gjeni më shumë informacion në lidhje me mënyrën e trajtimit të të dhënave të përdoruesit në Google Analytics në
                    Politika e privatësisë së Google: https://support.google.com/analytics/answer/6004245?hl=en

                </p>

                <h3 class="impressumPoint">Shtojcat e Facebook (si butoni)</h3>
                <p>Në faqet tona shtojcat e rrjetit social Facebook, ofruesi Facebook Inc., 1 Hacker Way, Menlo Park, California 94025, USA, janë të integruara. Shtojcat e Facebook mund të njihen me logon e Facebook ose butonin Like si në faqen tonë. Një përmbledhje e
                    Shtojcat e Facebook mund të gjenden këtu: https://developers.facebook.com/docs/plugins/.
                    Kur vizitoni faqet tona, shtojca do t'ju lidh direkt
                    Shfletuesi dhe serveri Facebook. Facebook merr informacionin që keni vizituar faqen tonë me adresën tuaj IP. Nëse klikoni në butonin Facebook Like ndërsa jeni regjistruar në llogarinë tuaj në Facebook, mund të lidhni përmbajtjen e faqeve tona në profilin tuaj të Facebook. Si rezultat, Facebook mund ta caktojë vizitën në faqet tona në llogarinë tuaj të përdoruesit. Theksojmë që ne si ofrues të faqeve nuk jemi të vetëdijshëm për përmbajtjen e të dhënave të transmetuara dhe përdorimin e tyre nga Facebook. Informacione të mëtejshme mund të gjenden në politikën e intimitetit të Facebook në
                    https://de-de.facebook.com/policy.php.
                    Nëse nuk dëshironi që Facebook të vizitojë faqet tona llogarinë tuaj të përdoruesit në Facebook
                    Ju lutemi dilni nga llogaria juaj e përdoruesit në Facebook.

                </p>

                <h3 class="impressumPoint">Encryption SSL</h3>
                <p>Kjo faqe përdor për arsye sigurie dhe për të mbrojtur transmetimin e përmbajtjes konfidenciale, siç janë kërkesat që ju na dërgoni si një operator faqesh,
                    një kriptim SSL. Një lidhje e koduar tregohet nga shiriti i adresës së shfletuesit duke ndryshuar nga "http: //" në "https: //" dhe ikonën e kyçjes në shiritin tuaj të shfletuesit. Nëse encryption SSL është aktivizuar, të dhënat që ju na paraqisni nuk mund të lexohen nga palët e treta.
                    E drejta për informim, anulim, bllokim
                    Ju keni të drejtë të lironi informacione mbi të dhënat tuaja personale të ruajtura, origjinën dhe marrësin e tyre dhe qëllimin e përpunimit të të dhënave, si dhe një të drejtë për korrigjimin, bllokimin ose fshirjen e këtyre të dhënave. Për më shumë informacion mbi të dhënat personale, ju lutemi na kontaktoni në çdo kohë në adresën e dhënë në shtyp.


                </p>
                <h3 class="impressumPoint">E-mail reklamimi të kundërshtimit</h3>
                <p>Përdorimi i publikuar në kontekstin e informacionit të kontaktit për detyrimin e ngulitur për transmetimin e reklamave dhe materialeve informative jo të kërkuara në mënyrë të qartë, kundërshtohet. Operatorët e faqeve shprehimisht rezervojnë të drejtën për të ndërmarrë veprime juridike në rast të dërgimit të pakërkuar të informacionit për reklamim, siç janë postë elektronike.

                </p>
                <h3 class="impressumPoint">Kontakte</h3>
                <p>Përmbajtja e tekstit dhe imazhi</p>
                <ul>
                    <li class="impressumInfos">Maelys Sh.P.K</li>
                    <li class="impressumInfos">Rr. Deshmoret e Kombit</li>
                    <li class="impressumInfos">51000 Decan</li>
                    <li class="impressumInfos">Telefoni 049111369</li>
                    <li class="impressumInfos">Viber +38349111369</li>
                    <li class="impressumInfos">E-Mail: info@maelys-ks.com</li>
                    <li class="impressumInfos">http:www.maelys-ks.com</li>
                    <li class="impressumInfos">Regjistri Tregtar Nr.Biznesit.: 810150269</li>
                    <li class="impressumInfos">Juridiksioni: Prishtine</li><br>
                    <li class="impressumInfos">Identifikimi i TVSH-së.Nr .: 330398653</li>
                </ul>

            </div>
            <!--col-md-10-->
        </div>
        <!--row-->
    </div>
    <!--container-->
</section>
<!-- /MAIN WRAP CONTENT -->
<?php
include("include/footer.php"); ?>