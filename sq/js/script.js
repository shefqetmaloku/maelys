$(document).ready(function() {
    $("header .icon").click(function() {
        if (!$(this).hasClass("active")) {
            $(this).addClass("active")
            $("#menu-links-header").addClass("active")
                // $("body").addClass("stop-scrolling")
        } else {
            $(this).removeClass("active")
            $("#menu-links-header").removeClass("active")
                // $("body").removeClass("stop-scrolling")
        }
    })

    var menuBorder = $('#menuBottom');
    $(window).scroll(function() {
        if ($(window).scrollTop() > 280) {
            menuBorder.addClass('show');
        } else {
            menuBorder.removeClass('show');
        }
    })
    var menuBorderMobile = $('#menuBottomMobile');
    $(window).scroll(function() {
            if ($(window).scrollTop() > 100) {
                menuBorderMobile.addClass('show');
            } else {
                menuBorderMobile.removeClass('show');

            }
        })
        // $(".span.arrowDown").click(function() {
        //     $('html,body').animate({
        //         scrollTop: $(".section.home-widget.home-features").offset().top + 250;
        //     }, 500);
        // })
    $("a.menu-item").click(function() {
        $('#menu-links-header').removeClass('active');
        $('.icon').removeClass('active');
    })
    $(function() {
        $('a#fb').hover(function() {
            $('i.fa.fa-facebook').css('color', '#100f12');

        }, function() {
            // on mouseout, reset the background colour
            $('i.fa.fa-facebook').css('color', '#e3b844');

        });
        $('a#insta').hover(function() {
            $('i.fa.fa-instagram').css('color', '#100f12');

        }, function() {
            // on mouseout, reset the background colour
            $('i.fa.fa-instagram').css('color', '#e3b844');

        });

    });

    function showApertizer() {
        var x = document.getElementsByClassName(".col-md-6.leftDiv.starters");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
    // Cookie Compliancy BEGIN
    function GetCookie(name) {
        var arg = name + "=";
        var alen = arg.length;
        var clen = document.cookie.length;
        var i = 0;
        while (i < clen) {
            var j = i + alen;
            if (document.cookie.substring(i, j) == arg)
                return "here";
            i = document.cookie.indexOf(" ", i) + 1;
            if (i == 0) break;
        }
        return null;
    }

    function testFirstCookie() {
        var offset = new Date().getTimezoneOffset();
        if ((offset >= -180) && (offset <= 0)) { // European time zones
            var visit = GetCookie("cookieCompliancyAccepted");
            if (visit == null) {
                $("#myCookieConsent").fadeIn(400); // Show warning
            } else {
                // Already accepted
            }
        }
    }
    $(document).ready(function() {
        $("#cookieButton").click(function() {
            console.log('Understood');
            var expire = new Date();
            expire = new Date(expire.getTime() + 7776000000);
            document.cookie = "cookieCompliancyAccepted=here; expires=" + expire + ";path=/";
            $("#myCookieConsent").hide(400);
        });
        testFirstCookie();
    });
    // Cookie Compliancy END
    //scroll to element and menu wont cover the content
    $('a.scrollDownLink').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $(this.hash).offset().top - 100
        }, 100, function() {});
    })

});