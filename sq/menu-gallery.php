<?php

include("include/header.php"); ?>



<!-- TOP IMAGE HEADER -->

<section class="topSingleBkg topPageBkg">

    <div class="item-content-bkg">

        <div class="item-img" style="background-image:url('images/top-headers/Gallery-Header.jpg');"></div>

        <div class="inner-desc">

            <img class="galleryHeader" src="images/top-headers/Galeria-02-02.svg">

        </div>

    </div>

</section>



<!-- MAIN WRAP CONTENT -->

<section id="wrap-content" class="page-content menuGalleryContent">

    <div class="masonry-wrapper">

        <div class="masonry">

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/1.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/1.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/4.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/4.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/3.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/3.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/9.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/9.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/12.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/12.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/17.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/17.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/18.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/18.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/19.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/19.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/29.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/24.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/22.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/22.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/23.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/23.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/2.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/2.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/6.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/6.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/10.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/10.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/13.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/13.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/11.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/11.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/20.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/20.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/25.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/25.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/26.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/26.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/28.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/28.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/30.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/30.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/7.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/7.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/5.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/5.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/8.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/8.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/16.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/16.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/15.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/15.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/21.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/21.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/14.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/14.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/27.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/27.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            <div class="menu-post gallery-post">

                <a href="images/gallery/food/29.jpg" class="lightbox" rel="prettyPhoto[gallery]" title="Chocolate Muffins">

                    <div class="masonry-item">

                        <img src="images/gallery/food/29.jpg"  class="masonry-content">

                    </div>

                </a>

            </div>

            



        </div>



        <!-- /GALLERY -->



    </div>

    <!--col-md-12-->

    </div>

    <!--row-->

    </div>

    <!--container-->

</section>

<!-- /MAIN WRAP CONTENT -->





<?php

include "include/footer.php";

?>