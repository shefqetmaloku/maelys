<?php

include("include/headerspec.php"); ?>



<section class="ourMenuSection">

    <div class="container ourMenuDiv">

        <h1 class="menuLeftTitle">Our Menu</h3>

            <p class="ourMenuParagraph">Our menus highlight our unique focus on where our food comes from. Whether it's mouth-watering steak from our butcher,sustainably sourced seafood or freshly havested produce from our farm,each ingredient is transformed into something delightfully delicious. Enjoy!</p>

    </div>

</section>





<!-- MAIN WRAP CONTENT -->

<section class="menu-1col-content" id="menuSection">



    <div class="col-md-12 leftDiv starters">

        <div class="menuBackgroundDiv leftDiv" id="appetizersDiv"><img class="menuTopImage" src="images/menu/Appetizers.jpg"></div>

    </div>

    <div class="col-md-12 leftDiv salads">

        <div class="menuBackgroundDiv leftDiv" id="saladsDiv"><img class="menuTopImage" src="images/menu/Salads.jpg"></div>

    </div>

    <div class="col-md-12 leftDiv mainCourses">

        <div class="menuBackgroundDiv leftDiv" id="coursesImage"><img class="menuTopImage" src="images/menu/MainCourses.jpg"></div>

    </div>

    <div class="col-md-12 leftDiv drinks">

        <div class="menuBackgroundDiv leftDiv" id="drinksImage"><img class="menuTopImage" src="images/menu/Drinks.jpg"></div>

    </div>

    <div class="col-md-12 leftDiv desserts">

        <div class="menuBackgroundDiv leftDiv" id="dessertImage"><img class="menuTopImage" src="images/menu/Desserts.jpg"></div>

    </div>



    <div class="container">

        <div class="row">

            <div id="myBtnContainer">

                <button class="btn menuBtn active" onclick="filterSelection('starters')" id="menuAppertizer"> Starters</button>

                <button class="btn menuBtn" onclick="filterSelection('salads')"> Salads</button>

                <button class="btn menuBtn" onclick="filterSelection('mainCourses')" id="mainCourses"> Main Courses</button>

                <button class="btn menuBtn" onclick="filterSelection('drinks')" id="menuDrinks"> Drinks</button>

                <button class="btn menuBtn" onclick="filterSelection('desserts')" id="menuDesserts">Desserts</button>

            </div>

            <div class="container">

                <div class="row menuContent">

                    <div class="col-md-6 leftDiv starters">

                        <h1 class="menuLeftTitle">Appetizers</h1>

                        <div class="FoodItem">

                            <p class="menuFoodTitle">Fried Mozzarella&nbsp;&nbsp;|&nbsp;&nbsp;3,00€</p>

                            <p class="menuFoodDesc">(1 portion - 3 slices)</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Fried Potato balls&nbsp;&nbsp;|&nbsp;&nbsp;3,50€</p>

                            <p class="menuFoodDesc">(1 portion - 5 pieces)</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Oven fried cheese&nbsp;&nbsp;|&nbsp;&nbsp;3,50€</p>

                            <p class="menuFoodDesc">(1 portion)</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Homemade bread&nbsp;&nbsp;|&nbsp;&nbsp;3,50€</p>

                            <p class="menuFoodDesc">(1 portion)</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle ">Cold Appetizer - Ordever for 1 person&nbsp;&nbsp;|&nbsp;&nbsp;6,00€</p>

                            <p class="menuFoodDesc">(Prosciutto, mozzarella, homemade cheese, french salad, olives, sauce)</p>

                        </div>

                    </div>

                    <div class="col-md-6 leftDiv starters" id="rightDiv">

                        <h1 class="menuLeftTitle">Soups</h1>

                        <div class="FoodItem">

                            <p class="menuFoodTitle">Royal Creamy Broccoli Soup Maélys&nbsp;&nbsp;|&nbsp;&nbsp;3,00€</p>

                            <p class="menuFoodDesc">(Bouillon, broccoli, spices, cream, walnuts, cheese)</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Creamy Carrot Soup&nbsp;&nbsp;|&nbsp;&nbsp;2,50€</p>

                            <p class="menuFoodDesc">(Bouillon, carrots, spices, cream, bread cubes)</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Creamy Tomato Soup&nbsp;&nbsp;|&nbsp;&nbsp;2,50€</p>

                            <p class="menuFoodDesc">(Bouillon, tomato, onion, garlic, cream, spices)</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Beef Soup&nbsp;&nbsp;|&nbsp;&nbsp;3,00€</p>

                            <p class="menuFoodDesc">(Beef Bouillon, beef, vegetables, spices)</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Chicken Soup&nbsp;&nbsp;|&nbsp;&nbsp;2,50€</p>

                            <p class="menuFoodDesc">(Bouillon, chicken meat, vegetables, spices, parsley)</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle ">Fish Soup&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>

                            <p class="menuFoodDesc">(Bouillon, fish, tomato sauce, spices, garlic, vegetables)</p>

                        </div>



                    </div>

                    <div class="col-md-6 leftDiv salads">

                        <h1 class="menuLeftTitle">Salads</h1>

                        <div class="FoodItem">

                            <p class="menuFoodTitle">Royal Salad Maélys&nbsp;&nbsp;|&nbsp;&nbsp;4,50€</p>

                            <p class="menuFoodDesc">Lettuce, rukola, iceberg, radicchio, finokio, tomatini, orange, dresing sauce</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Mixed Salad&nbsp;&nbsp;|&nbsp;&nbsp;3,50€</p>

                            <p class="menuFoodDesc">Lettuce, tomatini, cucumber, tricolore peppers, olives, mozzarella, dresing sauce, pickled corn, red onion</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Caesar Salad&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>

                            <p class="menuFoodDesc">Lettuce, chicken steak, parmesan, dried cherry tomatoes, lemon, spices, bread cubes, dresing sauce</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Mozzarella Salad&nbsp;&nbsp;|&nbsp;&nbsp;3,50€</p>

                            <p class="menuFoodDesc">Mozzarella cheese, fresh tomatoes, olive sauce</p>

                        </div>

                    </div>

                    <div class="col-md-6 leftDiv salads" id="rightDiv">

                        <h1 class="menuLeftTitle">Salads</h1>

                        <div class="FoodItem">

                            <p class="menuFoodTitle">Rukola Salad&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>

                            <p class="menuFoodDesc">Rukola salad, garlic sauce, onion, parsley, almond, basil, spices, parmesan, dresing sauce</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Lettuce Salad&nbsp;&nbsp;|&nbsp;&nbsp;2,50€</p>

                            <p class="menuFoodDesc">Lettuce salad, spices, dresing sauce</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Tuna Salad&nbsp;&nbsp;|&nbsp;&nbsp;3,80€</p>

                            <p class="menuFoodDesc">Iceberg salad, radicchio, tomatini, tuna, onion, dresing sauce</p>

                        </div>



                    </div>

                    <div class="col-md-6 leftDiv mainCourses">

                        <h1 class="menuLeftTitle">Pasta</h1>

                        <div class="FoodItem">

                            <p class="menuFoodTitle">Royal Pappaedelle Maélys&nbsp;&nbsp;|&nbsp;&nbsp;5,00€</p>

                            <p class="menuFoodDesc">Pappardelle, beef steak, fresh vegetables, tomato sauce, various spices, parmesan</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Pappardelle with chicken & broccoli&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>

                            <p class="menuFoodDesc">Pappardelle, chicken steak, broccoli, basil sauce (pesto genovese), various spices, parmesan</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Tagliatelle with beef&nbsp;&nbsp;|&nbsp;&nbsp;4,50€</p>

                            <p class="menuFoodDesc">Tagliatelle, beef, spinach sauce, various spices, parmesan</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Vegetarian Tagliatelle with black truffle cream sauce&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>

                            <p class="menuFoodDesc">Tagliatelle, fresh vegetables, truffle creamy sauce, various spices, parmesan</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Penne or Spaghetti Bolognese&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>

                            <p class="menuFoodDesc">Penne, bolognese sauce, various spices, parmesan</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle ">Penne or Spaghetti Carbonara&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>

                            <p class="menuFoodDesc">Penne, prosciutto, mushrooms, sauce, parmesan</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle ">Penne or Spinach Spaghetti&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>

                            <p class="menuFoodDesc">Penne or Spinach Spaghetti</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle ">Penne or Spaghetti Napoli&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>

                            <p class="menuFoodDesc">Penne, tomato sauce, olive oil, parmesan</p>

                        </div>

                    </div>

                    <div class="col-md-6 leftDiv mainCourses" id="rightDiv">

                        <h1 class="menuLeftTitle">Pizza</h1>

                        <div class="FoodItem">

                            <p class="menuFoodTitle">Royal Pizza Maélys&nbsp;&nbsp;|&nbsp;&nbsp;8,00€</p>

                            <p class="menuFoodDesc">Tomato sauce, shredded mozzarella, smoked salmon, olives, zucchini, tomato, garlic, asparagus , oregano</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Pizza Quattro Formaggi&nbsp;&nbsp;|&nbsp;&nbsp;6,00€</p>

                            <p class="menuFoodDesc">Tomato sauce, shredded mozzarella, 4 types of cheese, olives, oregano</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Pizza Gorgonzola&nbsp;&nbsp;|&nbsp;&nbsp;6,00€</p>

                            <p class="menuFoodDesc">Tomato sauce, shredded mozzarella, mushrooms, gorgonzola, oregano</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Pizza frutti di mare&nbsp;&nbsp;|&nbsp;&nbsp;10,00€</p>

                            <p class="menuFoodDesc">Tomato sauce, shredded mozzarella, sea fruits, tricolore peppers, garlic, oregano, spices, tomato, rukola</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Pizza Tuna&nbsp;&nbsp;|&nbsp;&nbsp;5,50€</p>

                            <p class="menuFoodDesc">Tomato sauce, shredded mozzarella, tuna, onion, spices, oregano</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle ">Pizza Mozzarella&nbsp;&nbsp;|&nbsp;&nbsp;6,00€</p>

                            <p class="menuFoodDesc">Tomato sauce, shredded mozzarella, mozzarella slices, tomato, fresh basil, spices, oregano, garlic</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle ">Pizza Mexikana&nbsp;&nbsp;|&nbsp;&nbsp;6,50€</p>

                            <p class="menuFoodDesc">Tomato Sauce, shredded mozzarella, homemade sausage, chili pepper, spices, oregano</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle ">Pizza Prosciutto&nbsp;&nbsp;|&nbsp;&nbsp;4,50€</p>

                            <p class="menuFoodDesc">Tomato Sauce, shredded mozzarella, prosciutto, spices, oregano</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle ">Pizza Margherita&nbsp;&nbsp;|&nbsp;&nbsp;4,50€</p>

                            <p class="menuFoodDesc">Tomato Sauce, shredded mozzarella, spices, oregano</p>

                        </div>



                    </div>

                    <div class="col-md-6 leftDiv mainCourses">

                        <h1 class="menuLeftTitle">Steak</h1>

                        <div class="FoodItem">

                            <p class="menuFoodTitle">Royal Steak Maélys&nbsp;&nbsp;|&nbsp;&nbsp;13,50€</p>

                            <p class="menuFoodDesc">Steak, gorgonzola, fresh vegetables, mozzarella, spices, meat sauce (mushrooms), potato puree, carrots</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Pepe Verde Steak&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>

                            <p class="menuFoodDesc">Steak, fresh vegetables, spices, Pepe Verde sauce, potato puree, broccoli</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Natural Steak&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>

                            <p class="menuFoodDesc">Steak, fried potatoes, rice, spices</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Mozzarella Steak&nbsp;&nbsp;|&nbsp;&nbsp;12,50€</p>

                            <p class="menuFoodDesc">Steak, mozzarella, potato puree, peas, carrots, spices, meat sauce</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Steak with black truffle cream sauce&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>

                            <p class="menuFoodDesc">Steak, potato puree, peas, spinach, rice, black truffle cream sauce, spices</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle ">Steak with fresh mushrooms&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>

                            <p class="menuFoodDesc">Steak, fresh mushrooms, fresh vegetables, spices, meat sauce</p>

                        </div>

                    </div>

                    <div class="col-md-6 leftDiv mainCourses" id="rightDiv">

                        <h1 class="menuLeftTitle">Rib Eye Steak</h1>

                        <div class="FoodItem">

                            <p class="menuFoodTitle">Royal Rib Eye Steak Maélys&nbsp;&nbsp;|&nbsp;&nbsp;11,00€</p>

                            <p class="menuFoodDesc">Rib eye steak, gorgonzola, fresh vegetables, mozzarella, spices, meat sauce (mushrooms), potato puree, carrots</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Pepe Verde Rib Eye Steak&nbsp;&nbsp;|&nbsp;&nbsp;9,50€</p>

                            <p class="menuFoodDesc">Rib eye steak, fresh vegetables, spices, Pepe Verde sauce, potato puree, broccoli</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Natural Rib Eye Steak&nbsp;&nbsp;|&nbsp;&nbsp;9,50€</p>

                            <p class="menuFoodDesc">Rib eye steak, fried potatoes, rice, spices</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Mozzarella Rib Eye Steak&nbsp;&nbsp;|&nbsp;&nbsp;10,00€</p>

                            <p class="menuFoodDesc">Rib eye steak, mozzarella, potato puree, peas, carrots, spices, meat sauce</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Rib Eye Steak with black truffle cream &nbsp;&nbsp;|&nbsp;&nbsp;9,50€</p>

                            <p class="menuFoodDesc">Rib eye steak, potato puree, peas, spinach, rice, black truffle cream sauce, spices</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle ">Rib Eye Steak with fresh mushrooms&nbsp;&nbsp;|&nbsp;&nbsp;9,50€</p>

                            <p class="menuFoodDesc">Rib eye steak, fresh mushrooms, fresh vegetables, spices, meat sauce</p>

                        </div>



                    </div>

                    <div class="col-md-6 leftDiv mainCourses">

                        <h1 class="menuLeftTitle">Rump Steak</h1>

                        <div class="FoodItem">

                            <p class="menuFoodTitle">Royal Rump Steak Maélys&nbsp;&nbsp;|&nbsp;&nbsp;10,00€</p>

                            <p class="menuFoodDesc">Rump steak, gorgonzola, fresh vegetables, mozzarella, spices, meat sauce (mushrooms), potato puree, carrots</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Pepe Verde Rump Steak&nbsp;&nbsp;|&nbsp;&nbsp;9,00€</p>

                            <p class="menuFoodDesc">Rump steak, fresh vegetables, spices, Pepe Verde sauce, potato puree, broccoli</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Natural Rump Steak&nbsp;&nbsp;|&nbsp;&nbsp;9,00€</p>

                            <p class="menuFoodDesc">Rump steak, fried potatoes, rice, spices</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Mozzarella Rump Steak&nbsp;&nbsp;|&nbsp;&nbsp;9,50€</p>

                            <p class="menuFoodDesc">Rump steak, mozzarella, potato puree, peas, carrots, spices, meat sauce</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Rump Steak with black truffle cream sauce&nbsp;&nbsp;|&nbsp;&nbsp;9,00€</p>

                            <p class="menuFoodDesc">Rump steak, potato puree, peas, spinach, rice, black truffle cream sauce, spices</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle ">Rump Steak with fresh mushrooms&nbsp;&nbsp;|&nbsp;&nbsp;9,00€</p>

                            <p class="menuFoodDesc">Rump steak, fresh mushrooms, fresh vegetables, spices, meat sauce</p>

                        </div>

                    </div>

                    <div class="col-md-6 leftDiv mainCourses" id="rightDiv">

                        <h1 class="menuLeftTitle">Chicken Dishes</h1>

                        <div class="FoodItem">

                            <p class="menuFoodTitle">Royal Chicken Steak Maélys&nbsp;&nbsp;|&nbsp;&nbsp;8,00€</p>

                            <p class="menuFoodDesc">Chicken steak , pesta genovese sauce, paparedelle, potato puree, carrots, broccoli, spices</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Viennese Chicken Steak&nbsp;&nbsp;|&nbsp;&nbsp;7,00€</p>

                            <p class="menuFoodDesc">Chicken steak, spinach, mozzarella, fried potatoes, kitchen sauce, spices</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Chicken Roulade&nbsp;&nbsp;|&nbsp;&nbsp;7,00€</p>

                            <p class="menuFoodDesc">Chicken steak, spinach, shredded mozzarella, fried potatoes, kitchen sauce, fresh vegetables</p>

                        </div>

                        <div class="FoodItem FoodItemSecond" style="visibility: hidden;">

                            <p class="menuFoodTitle">Crem Soup Brokoli mbreterore Maelys</p>

                            <p class="menuFoodDesc">Crem Soup Brokoli mbreterore Maelys</p>

                        </div>

                        <div class="FoodItem FoodItemSecond" style="visibility: hidden;">

                            <p class="menuFoodTitle">Crem Soup Brokoli mbreterore Maelys</p>

                            <p class="menuFoodDesc">Crem Soup Brokoli mbreterore Maelys</p>

                        </div>

                        <div class="FoodItem FoodItemSecond" style="visibility: hidden;">

                            <p class="menuFoodTitle ">Crem Soup Brokoli mbreterore Maelys</p>

                            <p class="menuFoodDesc">Crem Soup Brokoli mbreterore Maelys</p>

                        </div>

                        <div class="FoodItem FoodItemSecond" style="visibility: hidden;">

                            <p class="menuFoodTitle ">Crem Soup Brokoli mbreterore Maelys</p>

                            <p class="menuFoodDesc">Crem Soup Brokoli mbreterore Maelys</p>

                        </div>



                    </div>

                    <div class="col-md-6 leftDiv mainCourses">

                        <h1 class="menuLeftTitle">Rissoto</h1>

                        <div class="FoodItem">

                            <p class="menuFoodTitle">Royal Risotto Maélys&nbsp;&nbsp;|&nbsp;&nbsp;4,50€</p>

                            <p class="menuFoodDesc">Basmati rice, fresh vegetables, tender fillet, black truffle cream sauce</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Butter Risotto&nbsp;&nbsp;|&nbsp;&nbsp;3,50€</p>

                            <p class="menuFoodDesc">Basmati rice, butter, various spices, parmesan</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Vegetarian Risotto&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>

                            <p class="menuFoodDesc">Basmati rice, fresh vegetables, sauce, parmesan</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Spinach Risotto&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>

                            <p class="menuFoodDesc">Basmati rice, spinach sauce, butter, chicken steak, various spices, parmesan</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Chicken Risotto&nbsp;&nbsp;|&nbsp;&nbsp;4,00€</p>

                            <p class="menuFoodDesc">Basmati rice,chicken steak, fresh vegetables, curry sauce, various spices, parmesan</p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle ">Seafood Risotto&nbsp;&nbsp;|&nbsp;&nbsp;7,00€</p>

                            <p class="menuFoodDesc">Basmati rice, seafood, tomato sauce, fresh vegetables, tomatini</p>

                        </div>

                    </div>

                    <div class="col-md-6 leftDiv mainCourses" id="rightDiv">

                        <h1 class="menuLeftTitle">Sea Food</h1>

                        <div class="FoodItem">

                            <p class="menuFoodTitle">Royal Shrimp Maélys&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Grill fried Shrimp&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Fried Squid&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Stuffed Squid&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Sea Bass (Levrek)&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle ">Gilt-head bream&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle ">Salmon&nbsp;&nbsp;|&nbsp;&nbsp;12,00€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle ">Trout fish&nbsp;&nbsp;|&nbsp;&nbsp;6,00€</p>

                            <p class="menuFoodDesc"></p>

                        </div>



                    </div>

                    <div class="col-md-6 leftDiv drinks">

                        <h1 class="menuLeftTitle">Drinks</h1>

                        <div class="FoodItem">
                            <p class="menuFoodTitle">Schwarzwälder Kirschwasser&nbsp;&nbsp;|&nbsp;&nbsp;2,20€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Williams-Christ-Birne&nbsp;&nbsp;|&nbsp;&nbsp;2,50€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Wald Himbeergeist&nbsp;&nbsp;|&nbsp;&nbsp;2,50€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Mirabell&nbsp;&nbsp;|&nbsp;&nbsp;2,20€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Schwarzwälder Zwetschgenwasser&nbsp;&nbsp;|&nbsp;&nbsp;2,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Ingwer Likör&nbsp;&nbsp;|&nbsp;&nbsp;2,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Gin&nbsp;&nbsp;|&nbsp;&nbsp;1,50€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">George Rum&nbsp;&nbsp;|&nbsp;&nbsp;1,50€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Verak&nbsp;&nbsp;|&nbsp;&nbsp;2,50€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Kruškovac Birne&nbsp;&nbsp;|&nbsp;&nbsp;4,50€</p>
                            <p class="menuFoodDesc"></p>
                        </div>

                    </div>

                    <div class="col-md-6 leftDiv drinks" id="rightDiv">

                        <h1 class="menuLeftTitle">Drinks</h1>
                        <div class="FoodItem">
                            <p class="menuFoodTitle ">Cassis Schwarzer Johanisbeer Likör&nbsp;&nbsp;|&nbsp;&nbsp;3,50€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Schwarzwälder Kirschenlikör&nbsp;&nbsp;|&nbsp;&nbsp;4,90€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Schwarzwälder Pfirischlikör&nbsp;&nbsp;|&nbsp;&nbsp;4,90€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Schoko-Kirsch Sahnelikör&nbsp;&nbsp;|&nbsp;&nbsp;5,90€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Schwarzwald Teufel&nbsp;&nbsp;|&nbsp;&nbsp;6,00€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Caipirinha Samira&nbsp;&nbsp;|&nbsp;&nbsp;4,90€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle">Pina Colada<&nbsp;&nbsp;|&nbsp;&nbsp;4,90€/p> <p class="menuFoodDesc">
                            </p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Mojito&nbsp;&nbsp;|&nbsp;&nbsp;4,90€</p>
                            <p class="menuFoodDesc"></p>
                        </div>
                        <div class="FoodItem FoodItemSecond">
                            <p class="menuFoodTitle ">Brandy Likör&nbsp;&nbsp;|&nbsp;&nbsp;4,50€</p>
                            <p class="menuFoodDesc"></p>
                        </div>



                    </div>

                    <div class="col-md-6 leftDiv desserts">

                        <h1 class="menuLeftTitle">Desserts</h1>

                        <div class="FoodItem">

                            <p class="menuFoodTitle">Chocolate Souffle&nbsp;&nbsp;|&nbsp;&nbsp;3,00€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Panna Cotta&nbsp;&nbsp;|&nbsp;&nbsp;2,00€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Tiramisu&nbsp;&nbsp;|&nbsp;&nbsp;2,00€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Kadaif&nbsp;&nbsp;|&nbsp;&nbsp;2,00€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Crepe with Nutella (3 pcs)&nbsp;&nbsp;|&nbsp;&nbsp;3,00€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Crepe with honey (3 pcs)&nbsp;&nbsp;|&nbsp;&nbsp;3,00€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Crepe with fruits (3 pcs)&nbsp;&nbsp;|&nbsp;&nbsp;3,00€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Kadaif Baklava&nbsp;&nbsp;|&nbsp;&nbsp;2,00€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Baklava with Ice cream&nbsp;&nbsp;|&nbsp;&nbsp;3,50€</p>

                            <p class="menuFoodDesc"></p>

                        </div>



                    </div>

                    <div class="col-md-6 leftDiv desserts" id="rightDiv">

                        <h1 class="menuLeftTitle">Ice Cream</h1>

                        <div class="FoodItem">

                            <p class="menuFoodTitle">Hazelnut&nbsp;&nbsp;|&nbsp;&nbsp;1,80€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Vanilla&nbsp;&nbsp;|&nbsp;&nbsp;1,80€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Stracciatella&nbsp;&nbsp;|&nbsp;&nbsp;1,80€ </p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Chocolate&nbsp;&nbsp;|&nbsp;&nbsp;1,80€ </p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Strawberry&nbsp;&nbsp;|&nbsp;&nbsp;1,80€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Caramel&nbsp;&nbsp;|&nbsp;&nbsp;1,80€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Banana&nbsp;&nbsp;|&nbsp;&nbsp;1,80€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Snickers&nbsp;&nbsp;|&nbsp;&nbsp;1,80€</p>

                            <p class="menuFoodDesc"></p>

                        </div>

                        <div class="FoodItem FoodItemSecond">

                            <p class="menuFoodTitle">Forest fruits&nbsp;&nbsp;|&nbsp;&nbsp;1,80€</p>

                            <p class="menuFoodDesc"></p>

                        </div>



                    </div>



                </div>

            </div>



            <script>

                filterSelection("starters")



                function filterSelection(c) {

                    var x, i;

                    x = document.getElementsByClassName("leftDiv");

                    if (c == "starters");

                    for (i = 0; i < x.length; i++) {

                        w3RemoveClass(x[i], "show");

                        if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");

                    }

                }



                function w3AddClass(element, name) {

                    var i, arr1, arr2;

                    arr1 = element.className.split(" ");

                    arr2 = name.split(" ");

                    for (i = 0; i < arr2.length; i++) {

                        if (arr1.indexOf(arr2[i]) == -1) {

                            element.className += " " + arr2[i];

                        }



                    }

                }



                function w3RemoveClass(element, name) {

                    var i, arr1, arr2;

                    arr1 = element.className.split(" ");

                    arr2 = name.split(" ");

                    for (i = 0; i < arr2.length; i++) {

                        while (arr1.indexOf(arr2[i]) > -1) {

                            arr1.splice(arr1.indexOf(arr2[i]), 1);

                        }

                    }

                    element.className = arr1.join(" ");

                }



                // Add active class to the current button (highlight it)

                var btnContainer = document.getElementById("myBtnContainer");

                var btns = btnContainer.getElementsByClassName("btn");

                for (var i = 0; i < btns.length; i++) {

                    btns[i].addEventListener("click", function() {

                        var current = document.getElementsByClassName("active ");

                        current[0].className = current[0].className.replace(" active", "");

                        $('button.btn.menuBtn').removeClass('active');

                        this.className += " active";



                    });



                }

                $(function(){

                    menuFunction();

                    $(window).bind("hashchange", menuFunction());

                    

                });



                function menuFunction(){

                    if(window.location.hash=="#drinksImage"){

                        filterSelection('drinks')

                        $("#menuDrinks").addClass('active');

                        $("#menuAppertizer").removeClass('active');

                    }

                    else if(window.location.hash=="#coursesImage"){

                        filterSelection('mainCourses')

                        $("#mainCourses").addClass('active');

                        $("#menuDrinks").removeClass('active');

                        $("#menuAppertizer").removeClass('active');



                    }

                    else if(window.location.hash=="#dessertImage"){

                        filterSelection('desserts')

                        $("#menuDesserts").addClass('active');

                        $("#mainCourses").removeClass('active');

                        $("#menuDrinks").removeClass('active');

                        $("#menuAppertizer").removeClass('active');





                    }

                    else{

                        $("#menuAppertizer").addClass('active');



                    }

                }



            </script>

            <div class="col-md-10 col-md-offset-1">

            </div>

            <!--row-->

        </div>

        <!--container-->

</section>

<!-- /MAIN WRAP CONTENT -->



<?php

include "include/footer.php";

?>