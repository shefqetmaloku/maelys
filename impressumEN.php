<?php

include("include/headerspec.php");

?>



<!-- TOP IMAGE HEADER -->

<section class="topSingleBkg topPageBkg impressum">

    <div class="item-content-bkg">

        <div class="item-img"></div>

    </div>

</section>

<!-- /TOP IMAGE HEADER -->



<!-- MAIN WRAP CONTENT -->

<section id="wrap-content" class="page-content bookContent impressumContent">

    <div class="container impressumContainer">

        <div class="row">

            <div class="col-md-10 col-md-offset-1">

                <h1 class="impressumTitle">Impressum</h1>

                <h3 class="impressumPoint">Privacy Policy</h3>

                <p>

                    Privacy Policy Operators of these sites take the protection of your personal information very seriously. We treat your personal information in a confidential manner and in accordance with legal regulations on data protection and this privacy policy. Use of our website is usually possible without providing personal information. As far as our sites are concerned, personal information (for example name, address or E-mail addresses) has been raised, this is done, as far as possible, on a freiwilliger basis. This data will not be disclosed to third parties without your explicit consent. Please note that transmitting data over the Internet (for example, when communicating by email) may have security vulnerabilities. A complete protection of data from access by third parties is not possible.

                </p>

                <h3 class="impressumPoint">Cookies</h3>

                <p>Websites partially use so-called cookies. Cookies do not harm your computer and do not contain viruses. Cookies are used to make our offer more friendly, effective and secure. Cookies are small text files that are stored on your computer and stored by your browser. Most of the cookies we use are so-called session cookies. They will be automatically deleted at the end of your visit. Other cookies remain stored on your device until you delete them. These cookies let us know your browser on your next visit. You may set your browser to be informed of the setting of cookies and allow cookies only in individual cases, accept cookies on specific occasions, or generally disable and enable automatic deletion of cookies when you close the browser. Disabling cookies may limit the functionality of this website. </p>



                <h3 class="impressumPoint">Server log files</h3>

                <p>The site provider automatically collects and stores information in the so-called server log files, which your browser automatically transmits to us. These are:

                    <ul>

                        <li class="impressumBullet">browser type and browser version</li>

                        <li class="impressumBullet">operating system used</li>

                        <li class="impressumBullet">Referrer URL</li>

                        <li class="impressumBullet">Host name of host computer</li>

                        <li class="impressumBullet">Server request time</li><br>

                    </ul>

                    This data cannot be shared with specific persons. A fusion of this data with other data sources will not take place. We reserve the right to add this data later to check if we are aware of concrete indications of illegal use.



                </p>



                <h3 class="impressumPoint">Contact</h3>

                <p>If you send us inquiries through the Contact Form, your details will be obtained from the Inquiry Form including the contact details you provided there for the purpose of processing Search and in the event of any further inquiries stored with us. We will not share this information without your consent.



                </p>



                <h3 class="impressumPoint">Browser plugin</h3>

                <p>You may prevent cookies from being stored with a corresponding setting of your browser software; however, we note that in this case you may not be able to use all of the features of this site on the Internet. Additionally, you may prevent the collection of cookie-generated data related to your use of the Website (including your IP address) in Google and the processing of such data by Google by downloading the potential browser plug-in under the link and install:

                    <strong class="linku">https://tools.google.com/dlpage/gaoptout?hl=de</strong><br>

                    Objection to data collection

                    You can prevent Google Analytics from collecting your data by clicking on the link below. An opt-in cookie will be set to prevent your data from being collected during future visits to this site: disable Google Analytics You can find more information about how Google Analytics handles user data in this Privacy Policy Google: https://support.google.com/analytics/answer/6004245?hl=en



                </p>



                <h3 class="impressumPoint">Facebook plugins (like button)</h3>

                <p>On our social networking site plugins Facebook, provider Facebook Inc., 1 Hacker Way, Menlo Park, California 94025, USA, are integrated. Facebook plugins can be recognized with the Facebook logo or the Like button on our site. A summary of Facebook Plugins can be found here: https://developers.facebook.com/docs/plugins/. When you visit our sites, the plugin will connect you directly to your Facebook browser and server. Facebook receives the information you have visited our site with your IP address. If you click on the Facebook Like button while logged in to your Facebook account, you can link the content of our pages to your Facebook profile. As a result, Facebook may assign a visit to our sites to your user account. We emphasize that we as site providers are not aware of the content of the transmitted data and their use by Facebook. Further information can be found in Facebook's privacy policy at https://de-de.facebook.com/policy.php. If you do not want Facebook to visit our sites your Facebook user account Please log out of your Facebook user account.



                </p>



                <h3 class="impressumPoint">SSL Encryption</h3>

                <p>This site uses for security reasons and to protect the transmission of confidential content, such as requests that you send us as a site operator, an SSL encryption. An encrypted link is indicated by the browser's address bar by changing from "http: //" to "https: //" and the lock icon on your browser bar. If SSL encryption is enabled, the data you submit to us cannot be read by third parties. Right to information, cancellation, blocking You have the right to release information about your personal data stored, its origin and recipient and the purpose of data processing, as well as a right to correct, block or delete such data. sheep

                </p>

                <h3 class="impressumPoint">Opposition advertising emails</h3>

                <p>The use published in the context of contact information for the embedded obligation to transmit advertisements and information not explicitly required is disputed. Website operators expressly reserve the right to take legal action in the event of unsolicited disclosure of advertising information, such as emails.

                </p>

                <h3 class="impressumPoint">Contact</h3>

                <p>Text content and image</p>

                <ul>

                    <li class="impressumInfos">Maelys LLC</li>

                    <li class="impressumInfos">Str. Deshmoret e Kombit</li>

                    <li class="impressumInfos">51000 Decan</li>

                    <li class="impressumInfos">Phone 049111369</li>

                    <li class="impressumInfos">Viber +38349111369</li>

                    <li class="impressumInfos">E-Mail: info@maelys-ks.com</li>

                    <li class="impressumInfos">http:www.maelys-ks.com</li>

                    <li class="impressumInfos">Business Register No. Business: 810150269</li>

                    <li class="impressumInfos">Jurisdiction: Prishtine</li><br>

                    <li class="impressumInfos">VAT identification.No .: 330398653</li>

                </ul>



            </div>

            <!--col-md-10-->

        </div>

        <!--row-->

    </div>

    <!--container-->

</section>

<!-- /MAIN WRAP CONTENT -->

<?php

include("include/footer.php"); ?>