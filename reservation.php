<?php

include("include/header.php");

?>



<!-- TOP IMAGE HEADER -->

<section class="topSingleBkg topPageBkg">

    <div class="item-content-bkg">

        <div class="item-img" style="background-image:url('images/top-headers/3.jpg');"></div>

        <div class="inner-desc">

            <img class="bookNowImg" src="images/top-headers/Bookyourtable.svg">

        </div>

    </div>

</section>

<!-- /TOP IMAGE HEADER -->



<!-- MAIN WRAP CONTENT -->

<section id="wrap-content" class="page-content bookContent">

    <div class="container">

        <div class="row">

            <div class="col-md-10 col-md-offset-1" id="reservationMain">



                <div class="page-holder custom-page-template reservation">

                    <div class="categ-name bookCat">

                        <h2 class="makeRes">Make a reservation</h2>

                    </div>

                    <p class="alignc bookMoto">We hope you have a wonderful time at our restaurant!</p>

                </div>



                <div class="reservation_txt">

                    <form method="post" id="reservation-form" action='include/reservation-process.php'>

                        <div class="row">

                            <div class="col-md-4">

                                <!-- <label>Name*</label> -->



                                <p><input type="text" name="author" class="reservation-fields" placeholder="Name*" required oninvalid="this.setCustomValidity('Please enter you name!')" oninput="this.setCustomValidity('')"/></p>

                            </div>

                            <div class="col-md-4">

                                <!-- <label>Email*</label> -->



                                <p><input type="email" name="email" class="reservation-fields" placeholder="Email*"required oninvalid="this.setCustomValidity('Please enter your email@domain')" oninput="this.setCustomValidity('')" /></p>

                            </div>

                            <div class="col-md-4">

                                <!-- <label>Phone*</label> -->



                                <p><input type="number"  name="phone" class="reservation-fields" placeholder="Phone*" required oninvalid="this.setCustomValidity('Please enter you phone number!')" oninput="this.setCustomValidity('')" /></p>

                            </div>

                        </div>

                        <!--end row-->

                        <div class="row">

                            <div class="col-md-4">

                                <!-- <label>Date*</label> -->



                                <p><input type="text" name="datepicker" autocomplete="off" id="datepicker" class="reservation-fields" size="30" placeholder="Date*" required/></p>

                            </div>

                            <div class="col-md-4">

                                <!-- <label>Time*</label> -->



                                <p>

                                    <select name="time" class="reservation-fields" required oninvalid="this.setCustomValidity('Please choose time!')" oninput="this.setCustomValidity('')">
                                        <option value="09:00">08:00</option>

                                        <option value="09:00">09:00</option>

                                        <option value="10:00">10:00</option>

                                        <option value="11:00">11:00</option>

                                        <option value="12:00">12:00</option>

                                        <option value="13:00">13:00</option>

                                        <option value="14:00">14:00</option>

                                        <option value="15:00">15:00</option>

                                        <option value="16:00">16:00</option>

                                        <option value="17:00">17:00</option>

                                        <option value="18:00">18:00</option>

                                        <option value="19:00">19:00</option>

                                        <option value="20:00">20:00</option>

                                        <option value="21:00">21:00</option>

                                        <option value="22:00">22:00</option>

                                    </select>

                                </p>

                            </div>

                            <div class="col-md-4">

                                <!-- <label>Seats*</label> -->



                                <p><input type="number" min="1" name="persons" class="reservation-fields" placeholder="Seats*" required oninvalid="this.setCustomValidity('Please select the number of seats!')" oninput="this.setCustomValidity('')"/></p>

                            </div>

                        </div>

                        <!--end row-->

                        <!-- <label>Special Requests</label> -->



                        <p><textarea name="message" id="message2" class="reservation-fields" cols="100" rows="4" tabindex="4" placeholder="Special Requests"></textarea></p>



                        <p class="antispam">Leave this empty: <input type="text" name="url" /></p>



                        <p class="alignc"><input type="submit" value="Book Now" id="bookSubmit" /></p>

                    </form>

                </div>

                <!-- reservation_txt -->



                <div id="output"></div>



            </div>

            <!--col-md-10-->

        </div>

        <!--row-->

    </div>
    <script>
        var dateToday = new Date();
        $(function() {
            $("#datepicker").datepicker({
                minDate: dateToday,
                beforeShowDay: function(date) {
                    var day = date.getDay();
                    return [(day != 0), ''];
                }
            });
        });
    </script>


    <!--container-->

</section>

<!-- /MAIN WRAP CONTENT -->



<?php

include("include/footer.php"); ?>