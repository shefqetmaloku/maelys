$(document).ready(function() {
    $("header .icon").click(function() { $(this).hasClass("active") ? ($(this).removeClass("active"), $("#menu-links-header").removeClass("active")) : ($(this).addClass("active"), $("#menu-links-header").addClass("active")) });
    var o = $("#menuBottom");
    $(window).scroll(function() { $(window).scrollTop() > 280 ? o.addClass("show") : o.removeClass("show") });
    var e = $("#menuBottomMobile");

    function n() {
        var o = (new Date).getTimezoneOffset();
        o >= -180 && o <= 0 && (null == function(o) { for (var e = o + "=", n = e.length, i = document.cookie.length, c = 0; c < i;) { var a = c + n; if (document.cookie.substring(c, a) == e) return "here"; if (0 == (c = document.cookie.indexOf(" ", c) + 1)) break } return null }("cookieCompliancyAccepted") && $("#myCookieConsent").fadeIn(400))
    }
    $(window).scroll(function() { $(window).scrollTop() > 100 ? e.addClass("show") : e.removeClass("show") }), $("a.menu-item").click(function() { $("#menu-links-header").removeClass("active"), $(".icon").removeClass("active") }), $(function() { $("a#fb").hover(function() { $("i.fa.fa-facebook").css("color", "#100f12") }, function() { $("i.fa.fa-facebook").css("color", "#e3b844") }), $("a#insta").hover(function() { $("i.fa.fa-instagram").css("color", "#100f12") }, function() { $("i.fa.fa-instagram").css("color", "#e3b844") }) }), $(document).ready(function() {
        $("#cookieButton").click(function() {
            console.log("Understood");
            var o = new Date;

            o = new Date(o.getTime() + 7776e6), document.cookie = "cookieCompliancyAccepted=here; expires=" + o + ";path=/", $("#myCookieConsent").hide(400)
        }), n()
    }), $("a.scrollDownLink").on("click", function(o) { o.preventDefault(), $("html, body").animate({ scrollTop: $(this.hash).offset().top - 100 }, 100, function() {}) })
});